/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "rubro")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Rubro.findAll", query = "SELECT r FROM Rubro r")
    , @NamedQuery(name = "Rubro.findById", query = "SELECT r FROM Rubro r WHERE r.id = :id")
    , @NamedQuery(name = "Rubro.findByDescripcion", query = "SELECT r FROM Rubro r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Rubro.findByJubilacion", query = "SELECT r FROM Rubro r WHERE r.jubilacion = :jubilacion")
    , @NamedQuery(name = "Rubro.findByIva", query = "SELECT r FROM Rubro r WHERE r.iva = :iva")
    , @NamedQuery(name = "Rubro.findByDescripcionbreve", query = "SELECT r FROM Rubro r WHERE r.descripcionbreve = :descripcionbreve")
    , @NamedQuery(name = "Rubro.findByDescripcionBreve", query = "SELECT r FROM Rubro r WHERE r.descripcionBreve = :descripcionBreve")})
public class Rubro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "jubilacion")
    private Short jubilacion;
    @Column(name = "iva")
    private Short iva;
    @Size(max = 1)
    @Column(name = "descripcionbreve")
    private String descripcionbreve;
    @Size(max = 255)
    @Column(name = "descripcion_breve")
    private String descripcionBreve;

    public Rubro() {
    }

    public Rubro(Long id) {
        this.id = id;
    }

    public Rubro(Long id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Short getJubilacion() {
        return jubilacion;
    }

    public void setJubilacion(Short jubilacion) {
        this.jubilacion = jubilacion;
    }

    public Short getIva() {
        return iva;
    }

    public void setIva(Short iva) {
        this.iva = iva;
    }

    public String getDescripcionbreve() {
        return descripcionbreve;
    }

    public void setDescripcionbreve(String descripcionbreve) {
        this.descripcionbreve = descripcionbreve;
    }

    public String getDescripcionBreve() {
        return descripcionBreve;
    }

    public void setDescripcionBreve(String descripcionBreve) {
        this.descripcionBreve = descripcionBreve;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rubro)) {
            return false;
        }
        Rubro other = (Rubro) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Rubro[ id=" + id + " ]";
    }
    
}
