package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class AtencionesDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String descripcion;

    private String abrev;

    private Boolean estado;

    public AtencionesDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public String getAbrev() {
        return abrev;
    }

    public void setAbrev(String abrev) {
        this.abrev = abrev;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

}
