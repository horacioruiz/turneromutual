/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import py.mutualmsp.mutual.socios.ws.dto.PublicidadDTO;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.entities.Persona;
import py.mutualmsp.mutual.socios.ws.entities.RangoRetiroCompra;
import py.mutualmsp.mutual.socios.ws.entities.RetiroPedido;
import py.mutualmsp.mutual.socios.ws.entities.Socio;

/**
 *
 * @author Hruiz
 */
@Stateless
@Path("retiroPedido")
public class RangoRetiroPedidoWS implements Serializable {

    @PersistenceContext
    private EntityManager em;
    Logger log = Logger.getLogger(PublicidadWS.class.getSimpleName());

    @GET
    @Path("listarTRUE")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<PublicidadDTO> getPublicidad() {
        Response<PublicidadDTO> response = new Response<>();
        PublicidadDTO socio = new PublicidadDTO();
        String sql = "Select * from turnero.publicidad WHERE estado=TRUE;";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                socio.setIdPublicidad(Long.parseLong(objects[0].toString()));
                socio.setLink(objects[1].toString());
                socio.setDescripcion(objects[2].toString());
                try {
                    socio.setEstado(Boolean.parseBoolean(objects[3].toString()));
                } catch (Exception e) {
                }
            }
            response.setCodigo(200);
            response.setData(socio);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("actualizarObtenerRangoSerie/{serie}/{nombre}/{idSocio}/{ci}")
//    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Long> actualizarObtenerRangoSerie(@PathParam("serie") String serie, @PathParam("nombre") String nombre,
            @PathParam("idSocio") String idSocio, @PathParam("ci") String ci) {
        Response<Long> response = new Response<>();
        boolean esSocio = verificarSocio(ci);
        try {
            try {
                RetiroPedido rp = new RetiroPedido();
                Socio socio = new Socio();
                Persona persona = new Persona();
                long numRegistro = obtenerRango(serie);
                if (serie.equalsIgnoreCase("CA")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("CA0" + numRegistro);
//                    } else {
                    rp.setNumero("CA" + numRegistro);
//                    }
                    rp.setSerie("CA");
                } else if (serie.equalsIgnoreCase("AH")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("AH0" + numRegistro);
//                    } else {
                    rp.setNumero("AH" + numRegistro);
//                    }
                    rp.setSerie("AH");
                } else if (serie.equalsIgnoreCase("RC")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("AH0" + numRegistro);
//                    } else {
                    rp.setNumero("RC" + numRegistro);
//                    }
                    rp.setSerie("RC");
                } else if (serie.equalsIgnoreCase("CR")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("CR0" + numRegistro);
//                    } else {
                    rp.setNumero("CR" + numRegistro);
//                    }
                    rp.setSerie("CR");
                } else if (serie.equalsIgnoreCase("SG")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("SG0" + numRegistro);
//                    } else {
                    rp.setNumero("SG" + numRegistro);
//                    }
                    rp.setSerie("SG");
                } else if (serie.equalsIgnoreCase("AD")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("AD0" + numRegistro);
//                    } else {
                    rp.setNumero("AD" + numRegistro);
//                    }
                    rp.setSerie("AD");
                } else if (serie.equalsIgnoreCase("RE")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("RE0" + numRegistro);
//                    } else {
                    rp.setNumero("RE" + numRegistro);
//                    }
                    rp.setSerie("RE");
                } else if (serie.equalsIgnoreCase("SD")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("SD0" + numRegistro);
//                    } else {
                    rp.setNumero("SD" + numRegistro);
//                    }
                    rp.setSerie("SD");
                } else if (serie.equalsIgnoreCase("SO")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("SO0" + numRegistro);
//                    } else {
                    rp.setNumero("SO" + numRegistro);
//                    }
                    rp.setSerie("SO");
                } else if (serie.equalsIgnoreCase("OC")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("OC" + numRegistro);
//                    }
                    rp.setSerie("OC");
                } else if (serie.equalsIgnoreCase("TL")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("TL" + numRegistro);
//                    }
                    rp.setSerie("TL");
                } else if (serie.equalsIgnoreCase("SR")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("SR" + numRegistro);
//                    }
                    rp.setSerie("SR");
                } else if (serie.equalsIgnoreCase("CE")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("CE" + numRegistro);
//                    }
                    rp.setSerie("CE");
                } else if (serie.equalsIgnoreCase("FR")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("FR" + numRegistro);
//                    }
                    rp.setSerie("FR");
                } else if (serie.equalsIgnoreCase("RS")) {
//                    if (numRegistro <= 9) {
//                        rp.setNumero("OC" + numRegistro);
//                    } else {
                    rp.setNumero("RS" + numRegistro);
//                    }
                    rp.setSerie("RS");
                }

                if (esSocio) {
                    try {
                        socio.setId(Long.parseLong(idSocio));
                        rp.setSocio(socio);
                    } catch (Exception ex) {
                    } finally {
                    }
                } else {
                    try {
                        persona.setId(Long.parseLong(idSocio));
                        rp.setPersona(persona);
                    } catch (Exception ex) {
                    } finally {
                    }
                }

//		rp.setFacturaClienteCab(null);
                rp.setCliente(nombre);
                rp.setCi(ci);
                rp.setEntregado(false);
                rp.setListo(false);
                rp.setPreparacion(true);
                rp.setLlamar(false);

                String pattern = "HH:mm:ss";
                // 1. LocalDate
                LocalTime now = LocalTime.now();
                rp.setHora(now.format(DateTimeFormatter.ofPattern(pattern)));
                rp.setFecha(new Date());

                em.persist(rp);

                response.setCodigo(200);
                response.setData(numRegistro);
                response.setMensaje("Proceso satisfactorio");
                log.info("Proceso satisfactorio");
            } catch (NoResultException e) {
                response.setCodigo(205);
                response.setData(0l);
                response.setMensaje("Error: " + e.getMessage());
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
        return response;
    }

    private long obtenerRango(String serie) {
        String sql = "Select * from turnero.rango_retiro_compra WHERE UPPER(serie)='" + serie.toUpperCase() + "';";
        try {
            log.info("SQL -> " + sql);
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            RangoRetiroCompra rrc = new RangoRetiroCompra();
            for (Object[] objects : list) {
                rrc.setIdRango(Long.parseLong(objects[0].toString()));
                rrc.setRangoActual(Long.parseLong(objects[1].toString()));
                rrc.setRangoFinal(Long.parseLong(objects[2].toString()));
                rrc.setRangoInicial(Long.parseLong(objects[3].toString()));
                rrc.setSerie(objects[4].toString());
            }
            long rangoActual = rrc.getRangoActual();
            rrc.setRangoActual(rangoActual + 1);
            em.merge(rrc);
            return rangoActual;
        } catch (Exception e) {
            e.printStackTrace();
            return 0l;
        }
    }

    private boolean verificarSocio(String ci) {
        boolean valor = false;
        String sql = "Select * from public.socio WHERE cedula LIKE '" + ci + "%';";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            if (list.size() > 0) {
                valor = true;
            }
            return valor;
        } catch (Exception e) {
            e.printStackTrace();
            return valor;
        }
    }

}
