/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import py.mutualmsp.mutual.socios.ws.dto.PublicidadDTO;
import py.mutualmsp.mutual.socios.ws.dto.Response;

/**
 *
 * @author Hruiz
 */
@Stateless
@Path("publicidad")
public class PublicidadWS implements Serializable {

    @PersistenceContext
    private EntityManager em;
    Logger log = Logger.getLogger(PublicidadWS.class.getSimpleName());

    @GET
    @Path("listarTRUE")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<PublicidadDTO>> getPublicidad() {
        Response<List<PublicidadDTO>> response = new Response<>();
        String sql = "Select * from turnero.publicidad WHERE estado=TRUE;";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<PublicidadDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                PublicidadDTO socio = new PublicidadDTO();
                socio.setIdPublicidad(Long.parseLong(objects[0].toString()));
                socio.setLink(objects[1].toString());
                socio.setDescripcion(objects[2].toString());
                try {
                    socio.setEstado(Boolean.parseBoolean(objects[3].toString()));
                } catch (Exception e) {
                }
                listHere.add(socio);
            }
            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

}
