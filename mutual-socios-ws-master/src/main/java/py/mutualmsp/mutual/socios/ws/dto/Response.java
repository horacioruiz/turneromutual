/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
@XmlRootElement
public class Response<T> {
    
    @Getter
    @Setter
    private T data;
    @Getter
    @Setter    
    private Integer codigo;
    @Getter
    @Setter    
    private String mensaje;
}
