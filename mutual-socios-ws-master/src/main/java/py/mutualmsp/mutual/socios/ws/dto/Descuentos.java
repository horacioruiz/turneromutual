/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class Descuentos implements Serializable {
    
    @Getter
    @Setter
    private Date fecha;
    @Getter
    @Setter    
    private String periodo;
    @Getter
    @Setter
    private Integer orden;
    @Getter
    @Setter
    private Integer idCuenta;
    @Getter
    @Setter
    private String descripcion;
    @Getter
    @Setter
    private Integer idMovimiento;
    @Getter
    @Setter
    private String tipo;
    @Getter
    @Setter
    private Double atraso;
    @Getter
    @Setter
    private Double cerrado;
    @Getter
    @Setter
    private Double giraduria;
    @Getter
    @Setter
    private Double ventanilla;
    @Getter
    @Setter
    private Double asignacion;
    @Getter
    @Setter
    private Integer idSocio;
    @Getter
    @Setter
    private String cedula;
    
    @Getter
    @Setter
    private Double diferencia;
    
    @Getter
    @Setter
    private Double descuentoMutual;
    
    
     
}
