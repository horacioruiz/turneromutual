/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.dto.SociosDto;
import py.mutualmsp.mutual.socios.ws.entities.Socio;
import py.mutualmsp.mutual.socios.ws.util.DBConnection;
import py.mutualmsp.mutual.socios.ws.util.MailService;
import py.mutualmsp.mutual.socios.ws.util.PasswordService;
import py.mutualmsp.mutual.socios.ws.util.RandomString;

/**
 *
 * @author hectorvillalba
 */

@Stateless
@Path("registro")
public class RegistroWS  implements Serializable {

    @PersistenceContext
    EntityManager em;
    
    @Inject
    MailService mailService;
    
    Logger log = Logger.getLogger("RegistroWS");
    
    @GET
    @Path("activar-cuenta/{id}")
    @Produces(MediaType.TEXT_HTML)
    public String activarCuentaSocio(@PathParam("id")Long id){
        String html = "";
        try {
            try{
                log.info("### Activar Cuenta ###");
                Socio socio = (Socio) em.createQuery("select s from Socio s where s.id = :id")
                        .setParameter("id", id)
                        .getSingleResult();
                log.info("Socio: " + socio.getNombre() +" " + socio.getApellido());
                socio.setAccesoMutual(true);
                //response.setCodigo(200);
                html = "<html>\n" +
                "<head>\n" +
                "<style>\n" +
                "#container{width:500px;height:400px;background:#fff;margin:auto;border-radius:5px\n" +
                "}\n" +
                "#cabecera{width:100%;height:40%;background:#fff;}\n" +
                ".logo{width:250px;height:160px;display:block;margin:auto}\n" +
                "</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"container\">\n" +
                "	<div id=\"cabecera\">\n" +
                "		<img class=\"logo\" src=\"http://192.81.130.21/logo.png\">\n" +
                "		<h1 style= \"color:#fff;\n" +
                "		font-style: bold;\n" +
                "		font-family: Clarendon Blk BT;\n" +
                "		text-align: center;\n" +
                "		text-shadow: -2px -2px 2px #000, 2px -2px 2px #000, -2px 2px 2px #000, 2px 2px 2px #000;\">BIENVENIDO</h1>\n" +
                "		<h3 style=\"text-align:center;\n" +
                "		font-size: 35px;\n" +
                "		color:black;\n" +
                "		font-family: Calibri (Cuerpo)\"><strong>"+socio.getNombre() + " " + socio.getApellido()+"</strong></h3>\n" +
                "		<h3 style= \"color:#black;\n" +
                "		font-style: bold;\n" +
                "		font-family: Comic Sans MS;\n" +
                "		text-align: center\">Ya puede acceder a la App Mutual Movil!</h3>\n" +
                "	</div>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>";
                //response.setData(html);
                        //"Bienvenido " + socio.getNombre() + " " + socio.getApellido() + " " + " ya puede acceder a Mutual Movil");
                //response.setMensaje("Proceso satisfactorio");
                log.info("Proceso satisfactorio");
            }catch(NoResultException e){
                log.info("Error : " + e.getMessage());
                //response.setCodigo(205);
                //response.setData("Error al ejecutar el servicio");
                //response.setMensaje("Error: " + e.getMessage());
                return html;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return html;
    }
    
    @POST
    @Path("registrar-socio")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SociosDto> registrarSocio(SociosDto sociosDto){
        Response<SociosDto> response = new Response<>();
        try {
            try{
                log.info("### Registrar Socio ###");
                Socio socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.idestadosocio = 20")
                        .setParameter("cedula", sociosDto.getCedula())
                        .getSingleResult();
                log.info("Socio: " + socio.getNombre() +  " " + socio.getApellido());
                log.info("Cedula: " + socio.getCedula());
                if (socio.getAccesoMutual() != null){
                    if (socio.getAccesoMutual()) {
                        response.setCodigo(205);
                        response.setData(sociosDto);
                        response.setMensaje("Su cuenta ya se encuentra activada ");
                        log.info("Su cuenta ya se encuentra activada " + socio.getNombre() + " " + socio.getApellido());
                        return response;                        
                    }else{
                        if (socio.getPinAccesoMutual() != null) {
                            if (!socio.getPinAccesoMutual().equals("")) {
                                response.setCodigo(205);
                                response.setData(sociosDto);
                                response.setMensaje("Debe activar su cuenta, con el link que se envio a su correo");
                                log.info("Debe activar su cuenta, con el link que se envio a su correo" + socio.getNombre() + " " + socio.getApellido() + " ci:" + 
                                        socio.getCedula());
                                return response;    
                            }
                         
                        }
                    }
                }
                socio.setFcmToken(sociosDto.getFcmToken());
                socio.setEmail(sociosDto.getEmail());
                socio.setAccesoMutual(false);
                socio.setPinAccesoMutual(PasswordService.getInstance().encrypt(sociosDto.getPinMutual()));
                
                em.merge(socio);
                mailService.enviarMailActivacion(socio);
                response.setCodigo(200);
                response.setData(sociosDto);
                response.setMensaje("Proceso satisfactorio");
                log.info("Proceso satisfactorio");
            }catch(NoResultException e){
                response.setCodigo(205);
                response.setData(sociosDto);
                response.setMensaje("Error: " + e.getMessage());
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
        return response;
    }
    
    
    @POST
    @Path("registrar-socio-sms")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SociosDto> registrarSocioSMS(SociosDto sociosDto){
        Response<SociosDto> response = new Response<>();
        try {
            try{
                log.info("### Registrar Socio SMS ###");
                Socio socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.idestadosocio = 20")
                        .setParameter("cedula", sociosDto.getCedula())
                        .getSingleResult();
                log.info("Socio: " + socio.getNombre() +  " " + socio.getApellido());
                log.info("Cedula: " + socio.getCedula());
                log.info("Telefono: " + sociosDto.getTelefono());
                if (socio.getAccesoMutual() != null){
                    if (socio.getAccesoMutual()) {
                        response.setCodigo(205);
                        response.setData(sociosDto);
                        response.setMensaje("Esta cedula ya esta registrada en la App Mutual Socios");
                        log.info("Esta cedula ya esta registrada en la App Mutual Socios ");
                        return response;                        
                    }
                }
                
                socio.setFcmToken(sociosDto.getFcmToken());
                socio.setTelefonocelular(sociosDto.getTelefono());
                socio.setAccesoMutual(true);
                socio.setPinAccesoMutual(PasswordService.getInstance().encrypt(sociosDto.getPinMutual()));
                
                sociosDto.setId(socio.getId());
                sociosDto.setNombre(socio.getNombre());
                sociosDto.setApellido(socio.getNombre());
                
                em.merge(socio);
                response.setCodigo(200);
                response.setData(sociosDto);
                response.setMensaje("Proceso satisfactorio");
                log.info("Proceso satisfactorio");
            }catch(NoResultException e){
                response.setCodigo(205);
                response.setData(sociosDto);
                response.setMensaje("Error: " + e.getMessage());
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
        return response;
    }
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SociosDto> loginSocio(SociosDto sociosDto){
        Response<SociosDto> response = new Response<>();
        try {
            try{
                Socio socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.pinAccesoMutual = :pin and s.idestadosocio = 20")
                        .setParameter("cedula", sociosDto.getCedula())
                        .setParameter("pin", PasswordService.getInstance().encrypt(sociosDto.getPinMutual()))
                        .getSingleResult();
                socio.setFcmToken(sociosDto.getFcmToken());
                sociosDto.setId(socio.getId());
                sociosDto.setNombre(socio.getNombre());
                sociosDto.setApellido(socio.getApellido());
                sociosDto.setTelefono(socio.getTelefonocelularprincipal());
                
                if (!socio.getAccesoMutual()) {
                    response.setCodigo(205);
                    response.setData(sociosDto);
                    response.setMensaje("Aun no activo su cuenta, por favor ingrese a su correo y active la cuenta");
                    return response;
                }
                em.merge(socio);
                
                response.setCodigo(200);
                response.setData(sociosDto);
                response.setMensaje("Proceso satisfactorio");
            }catch(NoResultException e){
                response.setCodigo(205);
                response.setData(sociosDto);
                response.setMensaje("Error: Parametros invalidos, no se encontro socio con esos datos");
                return response;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    
    @POST
    @Path("recuperar-pin/{nroDocumento}/{email}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> recuperarPin(@PathParam("nroDocumento") String nroDocumento,
                                         @PathParam("email")String email){
        Response<String> response = new Response<>();
        Socio socio = null;
        try {
            log.info("### Recuperar Pin de Acceso ###");
            log.info("Email: " + email);
            log.info("cedula: " + nroDocumento);
            try {
                socio = (Socio) em.createQuery("select s from Socio s where s.email = :email and s.cedula = :cedula and s.idestadosocio = 20")
                    .setParameter("email", email)
                    .setParameter("cedula", nroDocumento)
                    .getSingleResult();
            } catch (NoResultException e) {
                log.info("Parametros invalidos...!!!");
                response.setCodigo(205);
                response.setData("Datos invalidos");
                response.setMensaje("Parametros invalidos");
                return response;
            }
            em.merge(socio);
            mailService.enviarPinAcceso(socio);
            response.setCodigo(200);
            response.setData("Le enviamos el pin de acceso a su correo...!!!");
            response.setMensaje("Proceso satisfactorio");
            log.info("Proceso satisfactorio");                
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    @POST
    @Path("recuperar-pin-sms/{nroDocumento}/{celular}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> recuperarPinSMS(@PathParam("nroDocumento") String nroDocumento,
                                         @PathParam("celular")String celular){
        Response<String> response = new Response<>();
        Socio socio = null;
        try {
            log.info("### Recuperar Pin de Acceso por SMS ###");
            log.info("Celulars: " + celular);
            log.info("cedula: " + nroDocumento);
            try {
                socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.idestadosocio = 20")
                    .setParameter("cedula", nroDocumento)
                    .getSingleResult();
            } catch (NoResultException e) {
                log.info("Parametros invalidos...!!!");
                response.setCodigo(205);
                response.setData("Datos invalidos");
                response.setMensaje("Parametros invalidos");
                return response;
            }
            if (socio.getAccesoMutual() !=null) {
                if (!socio.getAccesoMutual()){
                    log.info("Debe registrarse primero...!!!");
                    response.setCodigo(205);
                    response.setData("Debe registrarse primero en la App Mutual Socios");
                    response.setMensaje("Debe registrarse primero en la App Mutual Socios");
                    return response;
                }
            }
            em.merge(socio);
            enviarSMS(socio, celular);
            response.setCodigo(200);
            response.setData("Le enviamos el pin de acceso a su teléfono...!!!");
            response.setMensaje("Proceso satisfactorio");
            log.info("Proceso satisfactorio");                
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    @POST
    @Path("actualizar-pin/{nroDocumento}/{pin}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> actualizarPin(@PathParam("nroDocumento") String nroDocumento,
                                         @PathParam("pin") String pin){
        Response<String> response = new Response<>();
        Socio socio = null;
        try {
            log.info("### Actualizar Pin de Acceso ###");
            try {
                socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.idestadosocio = 20")
                    .setParameter("cedula", nroDocumento)
                    .getSingleResult();
            } catch (NoResultException e) {
                response.setCodigo(205);
                response.setData("Datos invalidos");
                response.setMensaje("Parametros invalidos");
                return response;
            }
            socio.setPinAccesoMutual(PasswordService.getInstance().encrypt(pin));
            em.merge(socio);
            response.setCodigo(200);
            response.setData("Se ha actualizado el pin correctamente...!!!");
            response.setMensaje("Proceso satisfactorio");
            log.info("Proceso satisfactorio");                
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    
    
    private void enviarSMS(Socio socio, String telefono) throws ClassNotFoundException, SQLException, Exception {
        DBConnection dBConnection = new DBConnection();
        String res="";
        try {
            String password = RandomString.getAlphaNumericString(4);
            String message = "Su password provisorio es: " + password;
            
           socio.setPinAccesoMutual(PasswordService.getInstance().encrypt(password));
            em.merge(socio); 
           Statement statement = dBConnection.getConnection().createStatement();
           String sql = "SELECT sms.programar_sms('"+telefono+"'," +
            "'Bienvenido a la Mutual Nac. del MSP Y BS "+message+"'" +","  + socio.getId()+ "," + 477 + ");";
            log.info(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
              res=resultSet.getString(1);
              log.info("programar_sms: " + res);
            }
         }
       catch (SQLException ex) {
            System.err.println( ex.getMessage() );
       }finally{
            dBConnection.getConnection().close();
       }
               
    }
}
