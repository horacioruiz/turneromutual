/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ejb;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.mail.EmailException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import py.mutualmsp.mutual.socios.ws.entities.NecesidadAyuda;
import py.mutualmsp.mutual.socios.ws.entities.SolicitudAyuda;
import py.mutualmsp.mutual.socios.ws.util.Constants;
import py.mutualmsp.mutual.socios.ws.util.MailService;

/**
 *
 * @author hectorvillalba
 */


@Startup
@Singleton
public class EnviarReporteNocturnoEJB implements Serializable {
   
    @PersistenceContext 
    EntityManager em;
    
    @EJB
    MailService mailService;
    
    private static final Logger logger = Logger.getLogger("EnviarReporteNocturnoEJB");
    private byte[] content;
    private FileInputStream fis;
    
    @PostConstruct
    public void init(){
        logger.log(Level.INFO, "Inicializando EJB...!!!");
        //timeService.createIntervalTimer(1000, 1000, new TimerConfig());
    }
    
    
    @Schedule(hour = "23", minute = "59", timezone = "", info = "Generates Night Report.") 
    public void runTask() { 
        Date fecha = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.US);
        logger.info("fecha: " + dateFormat.format(fecha));
        List<NecesidadAyuda> listaNecesidad = em.createQuery("select n from NecesidadAyuda n ").getResultList();
        for (NecesidadAyuda necesidadAyuda : listaNecesidad) {
            //String sql = "select s from SolicitudAyuda s left join s.idNecesidad n where s.fecha = '"+dateFormat.format(fecha)+"' and n.descripcion like '"+necesidadAyuda.getDescripcion()+"'";
            String sql ="select s.* from solicitud_ayuda s left join necesidad_ayuda n on n.id = s.id_necesidad \n"  +
                        "where cast(fecha as date) = '"+dateFormat.format(fecha)+"' and n.descripcion = '"+necesidadAyuda.getDescripcion()+"' ";
            List<SolicitudAyuda> lista = em.createNativeQuery(sql,SolicitudAyuda.class)
                .getResultList();  
                if (lista.isEmpty()) 
                    continue;
            try {
                creandoExcel(lista);
            } catch (EmailException ex) {
                Logger.getLogger(EnviarReporteNocturnoEJB.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
    
    private void creandoExcel(List<SolicitudAyuda> lista) throws EmailException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Report");
        Row row = null;
        int here = 0;

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String tipoSolicitud = "";
        for (SolicitudAyuda solicitudAyuda : lista) {
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("mm");
                SimpleDateFormat sdfHM = new SimpleDateFormat("HH.mm");
                boolean cabecera = false;
                if (here == 0) {
                    //PARA AGREGAR TITULO
                    sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 9));
                    row = sheet.createRow(here);

                    XSSFCellStyle style = workbook.createCellStyle();
                    XSSFFont font = workbook.createFont();
                    font.setFontHeightInPoints((short) 1);
                    font.setBold(true);
                    style.setFont(font);
                    style.setAlignment(HorizontalAlignment.CENTER);

                    Cell cell1 = row.createCell(1);
                    row.setRowStyle(style);
                    cell1.setCellValue("PLANILLA DE SOLICITUDES");
                    cabecera = true;
                }
                if (cabecera) {
                    //PARA AGREGAR NOMBRE DE FUNCIONARIO
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue("");
                    Cell cell2 = row.createCell(2);
                    cell2.setCellValue("");
                    Cell cell3 = row.createCell(3);
                    cell3.setCellValue("");
                    Cell cell4 = row.createCell(4);
                    cell4.setCellValue("");
                    Cell cell5 = row.createCell(5);
                    cell5.setCellValue("");
                    Cell cell6 = row.createCell(6);
                    cell6.setCellValue("");
                    Cell cell7 = row.createCell(7);
                    cell7.setCellValue("");
                    Cell cell8 = row.createCell(8);
                    cell8.setCellValue("");
                    Cell cell9 = row.createCell(9);
                    cell9.setCellValue("");
                    Cell cell10 = row.createCell(10);
                    cell10.setCellValue("");
                    Cell cell11 = row.createCell(11);
                    cell11.setCellValue("");
                    cabecera = true;
                }
                if (cabecera) {
                    row = sheet.createRow(++here);
                    Cell cell1 = row.createCell(1);
                    cell1.setCellValue("FECHA");
                    Cell cell2 = row.createCell(2);
                    cell2.setCellValue("CEDULA");
                    Cell cell3 = row.createCell(3);
                    cell3.setCellValue("NOMBRES");
                    Cell cell4 = row.createCell(4);
                    cell4.setCellValue("APELLIDOS");
                    Cell cell5 = row.createCell(5);
                    cell5.setCellValue("TELEFONO");
                    Cell cell6 = row.createCell(6);
                    cell6.setCellValue("CIUDAD");
                    Cell cell7 = row.createCell(7);
                    cell7.setCellValue("DEPARTAMENTO");
                    Cell cell8 = row.createCell(8);
                    cell8.setCellValue("NECESIDAD");
                    Cell cell9 = row.createCell(9);
                    cell9.setCellValue("MOTIVO");
                    Cell cell10 = row.createCell(10);
                    cell10.setCellValue("ATENDIDO POR");
                    Cell cell11 = row.createCell(11);
                    cell11.setCellValue("ESTADO");
                    cabecera = false;
                }
                row = sheet.createRow(++here);
                Cell cell1 = row.createCell(1);
                cell1.setCellValue(formatter.format(solicitudAyuda.getFecha()));
                Cell cell2 = row.createCell(2);
                cell2.setCellValue(solicitudAyuda.getIdsocio().getCedula());
                Cell cell3 = row.createCell(3);
                cell3.setCellValue(solicitudAyuda.getIdsocio().getNombre().toUpperCase());
                Cell cell4 = row.createCell(4);
                cell4.setCellValue(solicitudAyuda.getIdsocio().getApellido().toUpperCase());
                Cell cell5 = row.createCell(5);
                cell5.setCellValue(solicitudAyuda.getTelefono());
                Cell cell6 = row.createCell(6);
                cell6.setCellValue(solicitudAyuda.getIdciudad().getDescripcion().toUpperCase());
                Cell cell7 = row.createCell(7);
                cell7.setCellValue(solicitudAyuda.getIddepartamento() == null ? "--" : solicitudAyuda.getIddepartamento().getDescripcion().toUpperCase());
                Cell cell8 = row.createCell(8);
                cell8.setCellValue(solicitudAyuda.getIdNecesidad() == null ? "--" : solicitudAyuda.getIdNecesidad().getDescripcion());
                Cell cell9 = row.createCell(9);
                cell9.setCellValue(solicitudAyuda.getIdMotivoSolicitud().getDescripcion());
                Cell cell10 = row.createCell(10);
                cell10.setCellValue(solicitudAyuda.getAtendidopor() == null ? "--" : solicitudAyuda.getAtendidopor().toUpperCase());
                Cell cell11 = row.createCell(11);
                cell11.setCellValue(solicitudAyuda.getEstado() == null ? "--" : solicitudAyuda.getEstado().toUpperCase());
                tipoSolicitud = solicitudAyuda.getIdNecesidad().getDescripcion();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        }
        SimpleDateFormat formatters = new SimpleDateFormat("yyyy-MM-dd_hhmmss");
        Date date = new Date(System.currentTimeMillis());
        String tmpFile = Constants.UPLOAD_DIR + "\\temp\\" + formatters.format(date) + ".xls";
        try (FileOutputStream outputStream = new FileOutputStream(tmpFile)) {
            workbook.write(outputStream);
            File file = new File(Constants.UPLOAD_DIR + "/temp/" + formatters.format(date) + ".xls");
            content = new byte[(int) file.length()];
            fis = new FileInputStream(file);
            fis.read(content); //read file into bytes[]
            fis.close();
            if (tipoSolicitud.contains("Asistencia")) {
                mailService.sendeEmail("Mutual Ayuda", 
                                        "Se adjunta lista de solicitudes de la App Mutual Ayuda ["+ tipoSolicitud+"]", 
                                        Arrays.asList("lia.ibanez@mutualmsp.org.py","serviciosoperativos@mutualmsp.org.py"), content, formatters.format(date) + ".xls",
                                         "Mutual Ayuda");
            }else if (tipoSolicitud.contains("de")) {
                mailService.sendeEmail("Mutual Ayuda", 
                                        "Se adjunta lista de solicitudes de la App Mutual Ayuda ["+ tipoSolicitud+"]", 
                                        Arrays.asList("franco.sotelo@mutualmsp.org.py","serviciosfinancieros@mutualmsp.org.py"), content, formatters.format(date) + ".xls",
                                        "Mutual Ayuda");
            }else  {
                mailService.sendeEmail("Mutual Ayuda", 
                                        "Se adjunta lista de solicitudes de la App Mutual Ayuda ["+ tipoSolicitud+"]", 
                                        Arrays.asList("diana.gaona@mutualmsp.org.py","serviciosfinancieros@mutualmsp.org.py"), content, formatters.format(date) + ".xls",
                                        "Mutual Ayuda");
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EnviarReporteNocturnoEJB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(EnviarReporteNocturnoEJB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
