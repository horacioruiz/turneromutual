/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "operacionfija")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operacionfija.findAll", query = "SELECT o FROM Operacionfija o"),
    @NamedQuery(name = "Operacionfija.findById", query = "SELECT o FROM Operacionfija o WHERE o.id = :id"),
    @NamedQuery(name = "Operacionfija.findByIdsocio", query = "SELECT o FROM Operacionfija o WHERE o.idsocio = :idsocio"),
    @NamedQuery(name = "Operacionfija.findByIdcuenta", query = "SELECT o FROM Operacionfija o WHERE o.idcuenta = :idcuenta"),
    @NamedQuery(name = "Operacionfija.findByIdestado", query = "SELECT o FROM Operacionfija o WHERE o.idestado = :idestado"),
    @NamedQuery(name = "Operacionfija.findByImporte", query = "SELECT o FROM Operacionfija o WHERE o.importe = :importe"),
    @NamedQuery(name = "Operacionfija.findByTasainteres", query = "SELECT o FROM Operacionfija o WHERE o.tasainteres = :tasainteres"),
    @NamedQuery(name = "Operacionfija.findByIdmoneda", query = "SELECT o FROM Operacionfija o WHERE o.idmoneda = :idmoneda"),
    @NamedQuery(name = "Operacionfija.findByPlazo", query = "SELECT o FROM Operacionfija o WHERE o.plazo = :plazo"),
    @NamedQuery(name = "Operacionfija.findByFechaoperacion", query = "SELECT o FROM Operacionfija o WHERE o.fechaoperacion = :fechaoperacion"),
    @NamedQuery(name = "Operacionfija.findByIdpromotor", query = "SELECT o FROM Operacionfija o WHERE o.idpromotor = :idpromotor"),
    @NamedQuery(name = "Operacionfija.findByFechaprimervencimiento", query = "SELECT o FROM Operacionfija o WHERE o.fechaprimervencimiento = :fechaprimervencimiento"),
    @NamedQuery(name = "Operacionfija.findByFechavencimiento", query = "SELECT o FROM Operacionfija o WHERE o.fechavencimiento = :fechavencimiento"),
    @NamedQuery(name = "Operacionfija.findByObservacion", query = "SELECT o FROM Operacionfija o WHERE o.observacion = :observacion"),
    @NamedQuery(name = "Operacionfija.findByFechaanulado", query = "SELECT o FROM Operacionfija o WHERE o.fechaanulado = :fechaanulado"),
    @NamedQuery(name = "Operacionfija.findByObservacionanulado", query = "SELECT o FROM Operacionfija o WHERE o.observacionanulado = :observacionanulado"),
    @NamedQuery(name = "Operacionfija.findByFechadesactivado", query = "SELECT o FROM Operacionfija o WHERE o.fechadesactivado = :fechadesactivado"),
    @NamedQuery(name = "Operacionfija.findByObservaciondesactivado", query = "SELECT o FROM Operacionfija o WHERE o.observaciondesactivado = :observaciondesactivado"),
    @NamedQuery(name = "Operacionfija.findByFechacumplido", query = "SELECT o FROM Operacionfija o WHERE o.fechacumplido = :fechacumplido"),
    @NamedQuery(name = "Operacionfija.findByObservacioncumplido", query = "SELECT o FROM Operacionfija o WHERE o.observacioncumplido = :observacioncumplido"),
    @NamedQuery(name = "Operacionfija.findByNumerocuota", query = "SELECT o FROM Operacionfija o WHERE o.numerocuota = :numerocuota"),
    @NamedQuery(name = "Operacionfija.findByIdregional", query = "SELECT o FROM Operacionfija o WHERE o.idregional = :idregional"),
    @NamedQuery(name = "Operacionfija.findByNumerooperacion", query = "SELECT o FROM Operacionfija o WHERE o.numerooperacion = :numerooperacion"),
    @NamedQuery(name = "Operacionfija.findByImportetotal", query = "SELECT o FROM Operacionfija o WHERE o.importetotal = :importetotal"),
    @NamedQuery(name = "Operacionfija.findByReferencia", query = "SELECT o FROM Operacionfija o WHERE o.referencia = :referencia")})
public class Operacionfija implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsocio")
    private long idsocio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuenta")
    private long idcuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestado")
    private long idestado;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "importe")
    private BigDecimal importe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tasainteres")
    private BigDecimal tasainteres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmoneda")
    private long idmoneda;
    @Basic(optional = false)
    @NotNull
    @Column(name = "plazo")
    private short plazo;
    @Column(name = "fechaoperacion")
    @Temporal(TemporalType.DATE)
    private Date fechaoperacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idpromotor")
    private long idpromotor;
    @Column(name = "fechaprimervencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaprimervencimiento;
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechavencimiento;
    @Size(max = 50)
    @Column(name = "observacion")
    private String observacion;
    @Column(name = "fechaanulado")
    @Temporal(TemporalType.DATE)
    private Date fechaanulado;
    @Size(max = 50)
    @Column(name = "observacionanulado")
    private String observacionanulado;
    @Column(name = "fechadesactivado")
    @Temporal(TemporalType.DATE)
    private Date fechadesactivado;
    @Size(max = 50)
    @Column(name = "observaciondesactivado")
    private String observaciondesactivado;
    @Column(name = "fechacumplido")
    @Temporal(TemporalType.DATE)
    private Date fechacumplido;
    @Size(max = 50)
    @Column(name = "observacioncumplido")
    private String observacioncumplido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerocuota")
    private long numerocuota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idregional")
    private long idregional;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerooperacion")
    private long numerooperacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "importetotal")
    private BigDecimal importetotal;
    @Column(name = "referencia")
    private Long referencia;

    public Operacionfija() {
    }

    public Operacionfija(Long id) {
        this.id = id;
    }

    public Operacionfija(Long id, long idsocio, long idcuenta, long idestado, BigDecimal importe, BigDecimal tasainteres, long idmoneda, short plazo, long idpromotor, long numerocuota, long idregional, long numerooperacion, BigDecimal importetotal) {
        this.id = id;
        this.idsocio = idsocio;
        this.idcuenta = idcuenta;
        this.idestado = idestado;
        this.importe = importe;
        this.tasainteres = tasainteres;
        this.idmoneda = idmoneda;
        this.plazo = plazo;
        this.idpromotor = idpromotor;
        this.numerocuota = numerocuota;
        this.idregional = idregional;
        this.numerooperacion = numerooperacion;
        this.importetotal = importetotal;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(long idsocio) {
        this.idsocio = idsocio;
    }

    public long getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(long idcuenta) {
        this.idcuenta = idcuenta;
    }

    public long getIdestado() {
        return idestado;
    }

    public void setIdestado(long idestado) {
        this.idestado = idestado;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public BigDecimal getTasainteres() {
        return tasainteres;
    }

    public void setTasainteres(BigDecimal tasainteres) {
        this.tasainteres = tasainteres;
    }

    public long getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(long idmoneda) {
        this.idmoneda = idmoneda;
    }

    public short getPlazo() {
        return plazo;
    }

    public void setPlazo(short plazo) {
        this.plazo = plazo;
    }

    public Date getFechaoperacion() {
        return fechaoperacion;
    }

    public void setFechaoperacion(Date fechaoperacion) {
        this.fechaoperacion = fechaoperacion;
    }

    public long getIdpromotor() {
        return idpromotor;
    }

    public void setIdpromotor(long idpromotor) {
        this.idpromotor = idpromotor;
    }

    public Date getFechaprimervencimiento() {
        return fechaprimervencimiento;
    }

    public void setFechaprimervencimiento(Date fechaprimervencimiento) {
        this.fechaprimervencimiento = fechaprimervencimiento;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Date getFechaanulado() {
        return fechaanulado;
    }

    public void setFechaanulado(Date fechaanulado) {
        this.fechaanulado = fechaanulado;
    }

    public String getObservacionanulado() {
        return observacionanulado;
    }

    public void setObservacionanulado(String observacionanulado) {
        this.observacionanulado = observacionanulado;
    }

    public Date getFechadesactivado() {
        return fechadesactivado;
    }

    public void setFechadesactivado(Date fechadesactivado) {
        this.fechadesactivado = fechadesactivado;
    }

    public String getObservaciondesactivado() {
        return observaciondesactivado;
    }

    public void setObservaciondesactivado(String observaciondesactivado) {
        this.observaciondesactivado = observaciondesactivado;
    }

    public Date getFechacumplido() {
        return fechacumplido;
    }

    public void setFechacumplido(Date fechacumplido) {
        this.fechacumplido = fechacumplido;
    }

    public String getObservacioncumplido() {
        return observacioncumplido;
    }

    public void setObservacioncumplido(String observacioncumplido) {
        this.observacioncumplido = observacioncumplido;
    }

    public long getNumerocuota() {
        return numerocuota;
    }

    public void setNumerocuota(long numerocuota) {
        this.numerocuota = numerocuota;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }

    public long getNumerooperacion() {
        return numerooperacion;
    }

    public void setNumerooperacion(long numerooperacion) {
        this.numerooperacion = numerooperacion;
    }

    public BigDecimal getImportetotal() {
        return importetotal;
    }

    public void setImportetotal(BigDecimal importetotal) {
        this.importetotal = importetotal;
    }

    public Long getReferencia() {
        return referencia;
    }

    public void setReferencia(Long referencia) {
        this.referencia = referencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operacionfija)) {
            return false;
        }
        Operacionfija other = (Operacionfija) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Operacionfija[ id=" + id + " ]";
    }
    
}
