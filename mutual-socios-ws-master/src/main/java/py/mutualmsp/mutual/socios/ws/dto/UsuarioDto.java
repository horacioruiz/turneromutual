/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;
import javax.persistence.Column;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */

public class UsuarioDto implements Serializable {
    
    @Getter
    @Setter
    private String usuario;
    @Getter
    @Setter
    private String contrasena;
    
    @Getter
    @Setter
    private String nombreCompleto;
}
