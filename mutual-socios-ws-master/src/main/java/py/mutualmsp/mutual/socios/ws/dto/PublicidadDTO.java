package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;

//@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicidadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPublicidad;

    private String link;

    private String descripcion;

    private boolean estado;

//    private Boolean video;
//
//    private String tiempo;

    public PublicidadDTO() {
    }

    public Long getIdPublicidad() {
        return idPublicidad;
    }

    public void setIdPublicidad(Long idPublicidad) {
        this.idPublicidad = idPublicidad;
    }

    public String getLink() {
        return link;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

//    public Boolean getVideo() {
//        return video;
//    }
//
//    public void setVideo(Boolean video) {
//        this.video = video;
//    }

//    public String getTiempo() {
//        return tiempo;
//    }
//
//    public void setTiempo(String tiempo) {
//        this.tiempo = tiempo;
//    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

}
