/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import py.mutualmsp.mutual.socios.ws.dto.AhorroProgramadoCab;
import py.mutualmsp.mutual.socios.ws.dto.AhorroProgramadoDetalle;
import py.mutualmsp.mutual.socios.ws.dto.AtencionesDTO;
import py.mutualmsp.mutual.socios.ws.dto.CajaDTO;
import py.mutualmsp.mutual.socios.ws.dto.Descuentos;
import py.mutualmsp.mutual.socios.ws.dto.DescuentosCab;
import py.mutualmsp.mutual.socios.ws.dto.DetalleMovimiento;
import py.mutualmsp.mutual.socios.ws.dto.FuncionarioDTO;
import py.mutualmsp.mutual.socios.ws.dto.OrdenDeCompraCab;
import py.mutualmsp.mutual.socios.ws.dto.Prestamo;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.dto.SociosDto;
import py.mutualmsp.mutual.socios.ws.entities.Persona;

/**
 *
 * @author hectorvillalba
 */
@Stateless
@Path("consulta")
public class ConsultaSociosWS implements Serializable {

    @PersistenceContext
    private EntityManager em;
    Logger log = Logger.getLogger(ConsultaSociosWS.class.getSimpleName());

    @GET
    @Path("periodo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<String>> getPeriodo() {
        Response<List<String>> response = new Response<>();
        try {
            List<String> lista = new ArrayList<>();
            String sql = "select to_char(fechaproceso, 'YYYY-MM') from fechacierre  order by fechaproceso desc;";
            Query query = em.createNativeQuery(sql);
            List<String> list = query.getResultList();
            for (String objects : list) {
                //String param = objects[0].toString();
                lista.add(objects);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("socios/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SociosDto> getSocio(@PathParam("cedula") String cedula) {
        Response<SociosDto> response = new Response<>();
        SociosDto socio = new SociosDto();
        String sql = "select * from socio where cedula = :cedula";
        try {
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setNombre(objects[1].toString());
                socio.setApellido(objects[2].toString());
                try {
                    socio.setAccesoMutual(Boolean.parseBoolean(objects[30].toString()));
                } catch (Exception e) {
                }
                socio.setCedula(cedula);
            }
            response.setCodigo(200);
            response.setData(socio);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("personas/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Persona> getPersonas(@PathParam("cedula") String cedula) {
        Response<Persona> response = new Response<>();
        Persona socio = new Persona();
        String sql = "select * from persona where cedula like :cedula";
        try {
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula + "%");
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setCedula(objects[2].toString());
                socio.setNombre(objects[11].toString());
                try {
                    socio.setApellido(objects[12].toString());
                } catch (Exception e) {
                    socio.setApellido("");
                } finally {
                }
            }
            response.setCodigo(200);
            response.setData(socio);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("funcionarios/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<FuncionarioDTO> getFuncionarios(@PathParam("cedula") String cedula) {
        Response<FuncionarioDTO> response = new Response<>();
        FuncionarioDTO socio = new FuncionarioDTO();
        String sql = "select * from funcionario where cedula = :cedula";
        try {
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setNombre(objects[1].toString());
                socio.setApellido(objects[2].toString());
            }
            response.setCodigo(200);
            response.setData(socio);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("cajas")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<CajaDTO>> getCajas() {
        Response<List<CajaDTO>> response = new Response<>();
        String sql = "select * from turnero.cajas where estado=TRUE";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<CajaDTO> listCajaDTO = new ArrayList<>();
            for (Object[] objects : list) {
                CajaDTO socio = new CajaDTO();
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setDescripcion(objects[1].toString());
                socio.setEstado(Boolean.parseBoolean(objects[2].toString()));

                listCajaDTO.add(socio);
            }
            response.setCodigo(200);
            response.setData(listCajaDTO);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("atenciones")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<AtencionesDTO>> getAtenciones() {
        Response<List<AtencionesDTO>> response = new Response<>();
        String sql = "select * from turnero.atenciones where estado=TRUE";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<AtencionesDTO> listCajaDTO = new ArrayList<>();
            for (Object[] objects : list) {
                AtencionesDTO socio = new AtencionesDTO();
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setDescripcion(objects[1].toString());
                socio.setEstado(Boolean.parseBoolean(objects[2].toString()));
                socio.setAbrev(objects[3].toString());

                listCajaDTO.add(socio);
            }
            response.setCodigo(200);
            response.setData(listCajaDTO);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("descuentos/{idsocio}/{periodo}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<DescuentosCab> getDescuentos(@PathParam("idsocio") Integer idsocio,
            @PathParam("periodo") String periodo) {
        Response<DescuentosCab> response = new Response<>();
        List<Descuentos> lista = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        if (periodo.equals("")) {
            periodo += calendar.get(Calendar.YEAR) + "0" + calendar.get(Calendar.MONTH);
        }
        if (periodo.contains("-")) {
            periodo = periodo.replace("-", "");
        }
        Query queryFecha = em.createNativeQuery("select * from fechacierre f where  f.fechaproceso = "
                + " (SELECT cast(date_trunc('MONTH', cast('" + periodo + "'||'01' as date)) "
                + "+ INTERVAL '1 MONTH - 1 day' as date))");
        Object[] rangos = (Object[]) queryFecha.getSingleResult();

        String sql = "select fecha, periodo, orden, "
                + " descripcion, idmovimiento, tipo,"
                + " sum(atraso) as atraso, sum(cerrado) as cerrado, sum(giraduria) as giraduria, sum(ventanilla) as ventanilla, sum(asignacion) as asignacion, idsocio, cedula "
                + " from (SELECT cie.periodo as fecha, \n"
                + "  to_char(cie.periodo, 'TMMonth')||'/'||to_char(cie.periodo, 'YYYY') as periodo, 	\n"
                + " (CASE WHEN cie.tabla='of' THEN 1 	\n"
                + "  WHEN cie.tabla='js' THEN 2 	\n"
                + "  WHEN cie.tabla='ap' THEN 3 	\n"
                + "  WHEN cie.tabla='mo' THEN 4 	\n"
                + "  WHEN cie.tabla='ce' THEN 6 	\n"
                + "  ELSE 5 END) as orden, 	\n"
                + "  cie.idcuenta as idcuenta, 	\n"
                + "  cie.descripcion2 as descripcion,\n"
                + "  cie.idmovimiento as idmovimiento, \n"
                + "  cie.tipo as tipo, \n"
                + "  cast(cie.atraso as NUMERIC(18)) as atraso, \n"
                + "  cast(cie.cerrado as NUMERIC(18)) as cerrado, \n"
                + " cast((CASE WHEN gir.giraduria IS NOT NULL THEN gir.giraduria ELSE 0 END) as NUMERIC(18)) as giraduria, 	\n"
                + " cast((CASE WHEN gir.ventanilla IS NOT NULL THEN gir.ventanilla ELSE 0 END) as NUMERIC(18)) as ventanilla,\n"
                + " cast((CASE WHEN gir.asignacion IS NOT NULL THEN gir.asignacion ELSE 0 END) as NUMERIC(18)) as asignacion, \n"
                + "  cie.idsocio as idsocio, \n"
                + "  cie.cedula as cedula \n"
                + " FROM (select ci.fechacierre AS periodo, ci.idcuenta, 	\n"
                + "	   case when ci.idcuenta = 105 then 'OC ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion,'DD/MM/YY') 	\n"
                + "	   when ci.idcuenta = 98 then 'FINANCIERA ('||ent.descripcion ||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion, 'DD/MM/YY')      \n"
                + "	   when ci.idcuenta = 104 or ci.idcuenta = 35 then 'PRESTAMO MUTUAL (' ||tc.descripcion||') - Op.'||ci.numerooperacion||' - '||TO_CHAR(ci.fechaoperacion, 'DD/MM/YY') 	\n"
                + "	   else cu.descripcion||' - Op.'||ci.numerooperacion||' - '||coalesce(TO_CHAR(ci.fechaoperacion, 'DD/MM/YY'), TO_CHAR(ci.fechacierre, 'DD/MM/YY')) end as descripcion2,\n"
                + "	   ci.idmovimiento, ci.tabla, gi.descripcionbreve||' '||ru.descripcionbreve as tipo, 	\n"
                + "	   cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') < TO_CHAR(ci.fechacierre,'YYYYMM00') 	then ci.saldo else 0 end) as numeric(12)) ATRASO, 	\n"
                + "	   cast(sum(case when TO_CHAR(ci.fechavencimiento,'YYYYMM00') = TO_CHAR(ci.fechacierre,'YYYYMM00') 	then ci.saldo else 0 end) as numeric(12)) CERRADO, 	\n"
                + "	   0 as giraduria, 0 as ventanilla, 0 as asignacion, ci.cedula, ci.idsocio 	\n"
                + "	   from cierre as ci \n"
                + "	   left join cuenta as cu on cu.id = ci.idcuenta \n"
                + "	   left join giraduria as gi on gi.id = ci.idgiraduria 	\n"
                + "	   left join rubro as ru on ru.id = ci.idrubro \n"
                + "	   left join movimiento as mo on mo.id = ci.idmovimiento and ci.tabla='mo' \n"
                + "	   left join entidad as ent on ent.id = mo.identidad \n"
                + "	   LEFT join tipocredito as tc on tc.id = mo.idtipocredito \n"
                + "	   where 0=0 	\n"
                + "	   and TO_CHAR(ci.fechacierre, 'YYYYMM00') = '" + periodo + "00' \n"
                + "	   and ci.idsocio = " + idsocio + " 	\n"
                + "	   group by ci.idsocio, ci.cedula, ci.fechacierre, ci.idcuenta, descripcion2, ci.idmovimiento, ci.tabla, tipo \n"
                + "	   order by ci.fechacierre, ci.cedula, ci.idcuenta) as CIE \n"
                + " LEFT JOIN (select ci.fechacierre AS periodo, di.idcuenta, cu.descripcion, ci.idmovimiento, ci.tabla, 0 AS ATRASO, 0 as CERRADO, 	\n"
                + "			cast(sum(case when p.idviacobro = 38 and (i.fechaaaplicar = '" + rangos[1].toString() + "' and TO_CHAR(ci.fechacierre, 'YYYYMM00') = '" + periodo + "00' ) \n"
                + "					 then di.montocobro else 0 end) as numeric(12)) GIRADURIA, \n"
                + "			cast(sum(case when (p.idviacobro = 39 and (select idtipovalor from detallevalor where idingreso=i.id order by idtipovalor limit 1) <> 10\n"
                + "								and (select idtipovalor from detallevalor where idingreso=i.id order by idtipovalor limit 1) <> 9)\n"
                + "					 and i.fechaaaplicar >= '" + rangos[6].toString() + "' and i.fechaaaplicar <= '" + rangos[7].toString() + "' and TO_CHAR(ci.fechacierre, 'YYYYMM00') = '" + periodo + "00' \n"
                + "					 then di.montocobro else 0 end) as numeric(12)) VENTANILLA, \n"
                + "			cast(sum(case when ((p.idviacobro = 39 OR p.idviacobro = 221) and (select idtipovalor from detallevalor where idingreso=i.id order by idtipovalor limit 1) = 9)\n"
                + "					 and i.fechaaaplicar >= '" + rangos[6].toString() + "' and i.fechaaaplicar <= '" + rangos[7].toString() + "' and TO_CHAR(ci.fechacierre, 'YYYYMM00') = '" + periodo + "00'   \n"
                + "					 then di.montocobro else 0 end) as numeric(12)) ASIGNACION, \n"
                + "			i.idsocio, so.cedula \n"
                + "			from cierre as ci 	\n"
                + "			right join detalleingreso as di on di.iddetalleoperacion = ci.iddetalleoperacion and di.idcuenta = ci.idcuenta \n"
                + "			left join ingreso as i on i.id = di.idingreso 	\n"
                + "			left join planillaingreso as p on p.id = i.idplanilla 	\n"
                + "			left join cuenta as cu on di.idcuenta=cu.id 	\n"
                + "			left join socio as so on so.id = i.idsocio 	\n"
                + "			left join movimiento as mo on mo.id = di.idmovimiento and di.tabla='mo' \n"
                + "			left join entidad as ent on ent.id = mo.identidad \n"
                + "			LEFT join tipocredito as tc on tc.id = mo.idtipocredito 	\n"
                + "			where 0=0 and i.idestado = 146 	and TO_CHAR(ci.fechacierre, 'YYYYMM00') = '" + periodo + "00' 	\n"
                + "			and ci.idsocio = " + idsocio + "  	\n"
                + "			group by i.idsocio, so.cedula, ci.fechacierre, di.idcuenta, cu.descripcion, ci.idmovimiento, ci.tabla 	\n"
                + "			order by ci.fechacierre, so.cedula, di.idcuenta) as GIR ON gir.periodo = cie.periodo and gir.idcuenta = cie.idcuenta and cie.idsocio = gir.idsocio \n"
                + " 			and cie.idmovimiento = gir.idmovimiento \n"
                + " union	(select ag.periodo as fecha, to_char(ag.periodo, 'TMMonth')||'/'||to_char(ag.periodo, 'YYYY') as periodo, 7 as orden, 0 as idcuenta,\n"
                + "		 'SOBRANTE (APLICAR O REEMBOLSAR)' as descripcion, 0 as idmovimiento, '--' as tipo, 0 as atraso, 0 as cerrado,     \n"
                + "		 cast(sum(ag.monto-ag.montoaplicado) as numeric(12)) as GIRADURIA, 0 as ventanilla, 0 as asignacion, so.id, so.cedula      \n"
                + "		 from archivogiraduria ag     \n"
                + "		 inner join socio so on so.cedula = ag.cedula and so.id = " + idsocio + "    \n"
                + "		 where (ag.monto - ag.montoaplicado > 0)       \n"
                + "		 and TO_CHAR(ag.periodo, 'YYYYMM00') = '" + periodo + "00'    \n"
                + "		 group by ag.periodo, so.id, so.cedula)  \n"
                + " ORDER BY fecha, orden, idmovimiento) as detalle "
                + "group by  fecha, periodo, orden, descripcion, idmovimiento, tipo,idsocio, cedula"
                + " order by fecha, orden, idmovimiento ";
        log.info("Sql: " + sql);
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            DescuentosCab descuentosCab = new DescuentosCab();
            double totalDescMututal = 0.0;
            double totalGiraduria = 0.0;
            double totalVentanilla = 0.0;
            double totalAsignacion = 0.0;
            double totalDiferencia = 0.0;
            for (Object[] objects : list) {
                Descuentos descuentos = new Descuentos();
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(objects[0].toString());
                descuentos.setFecha(date);
                descuentos.setPeriodo(objects[1].toString());
                descuentos.setOrden(Integer.parseInt(objects[2].toString()));
                // descuentos.setIdCuenta(Integer.parseInt(objects[3].toString()));
                descuentos.setDescripcion(objects[3].toString());
                descuentos.setIdMovimiento(Integer.parseInt(objects[4].toString()));
                descuentos.setTipo(objects[5].toString());
                descuentos.setAtraso(Double.parseDouble(objects[6].toString()));
                descuentos.setCerrado(Double.parseDouble(objects[7].toString()));
                descuentos.setGiraduria(Double.parseDouble(objects[8].toString()));
                descuentos.setVentanilla(Double.parseDouble(objects[9].toString()));
                descuentos.setAsignacion(Double.parseDouble(objects[10].toString()));
                descuentos.setIdSocio(Integer.parseInt(objects[11].toString()));
                descuentos.setCedula(objects[12] == null ? "" : objects[12].toString());
                descuentos.setDescuentoMutual(descuentos.getCerrado() + descuentos.getAtraso());
                descuentos.setDiferencia((descuentos.getAtraso() + descuentos.getCerrado()) - (descuentos.getGiraduria() + descuentos.getVentanilla() + descuentos.getAsignacion()));
                totalDescMututal += descuentos.getDescuentoMutual();
                totalGiraduria += descuentos.getGiraduria();
                totalVentanilla += descuentos.getVentanilla();
                totalAsignacion += descuentos.getAsignacion();
                totalDiferencia += descuentos.getDiferencia();
                lista.add(descuentos);
            }
            descuentosCab.setListaDescuentos(lista);
            descuentosCab.setTotalAsignacion(totalAsignacion);
            descuentosCab.setTotalDescMututal(totalDescMututal);
            descuentosCab.setTotalDiferencia(totalDiferencia);
            descuentosCab.setTotalGiraduria(totalGiraduria);
            descuentosCab.setTotalVentanilla(totalVentanilla);

            response.setCodigo(200);
            response.setData(descuentosCab);
            response.setMensaje("Proceso satisactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("ahorro-cab/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<AhorroProgramadoCab>> getAhorroProgramadoCab(@PathParam("cedula") String cedula) {
        Response<List<AhorroProgramadoCab>> response = new Response<>();
        List<AhorroProgramadoCab> lista = new ArrayList<>();
        String sql = " select ap.id, ap.numerooperacion,\n"
                + "               ap.contrato, \n"
                + "               ap.plazo,\n"
                + "               ap.tasainteres,\n"
                + "               ap.fechaoperacion,\n"
                + "               ap.importe, \n"
                + "			 sum(dop.montocapital - dop.saldocapital) as montoaportado\n"
                + "             from ahorroprogramado ap \n"
                + "             left join detalleoperacion dop on dop.idmovimiento=ap.id and dop.tabla='ap'\n"
                + "             left join socio so on so.id=ap.idsocio \n"
                + "             where ap.idestado=53 \n"
                + "             and so.cedula= :cedula \n"
                + "             group by ap.id,ap.numerooperacion,ap.contrato, ap.plazo,ap.tasainteres,ap.fechaoperacion,ap.importe           \n"
                + "             order by ap.fechaoperacion,ap.numerooperacion";
        try {
            log.info("### AhorroCab ###");
            log.info("Cedula: " + cedula);
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula);
            List<Object[]> list = query.getResultList();
            if (list.isEmpty()) {
                response.setCodigo(205);
                response.setMensaje("No se encontraron datos");
                return response;
            }
            for (Object[] objects : list) {
                AhorroProgramadoCab ahorroProgramadoCab = new AhorroProgramadoCab();
                ahorroProgramadoCab.setId(Long.valueOf(objects[0].toString()));
                ahorroProgramadoCab.setNumeroOperacion(Long.parseLong(objects[1].toString()));
                ahorroProgramadoCab.setContrato(objects[2].toString());
                ahorroProgramadoCab.setPlazo(Integer.parseInt(objects[3].toString()));
                ahorroProgramadoCab.setTasainteres(Double.parseDouble(objects[4].toString()));
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(objects[5].toString());
                ahorroProgramadoCab.setFechaOperacion(date);
                ahorroProgramadoCab.setImporte(Double.parseDouble(objects[6].toString()));
                ahorroProgramadoCab.setMontoAportado(Double.parseDouble(objects[7].toString()));
                lista.add(ahorroProgramadoCab);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setData(lista);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("ahorro-detalle/{idmovimiento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<AhorroProgramadoDetalle>> getAhorroProgramadoDet(@PathParam("idmovimiento") Integer idmovimiento) {
        Response<List<AhorroProgramadoDetalle>> response = new Response<>();
        List<AhorroProgramadoDetalle> lista = new ArrayList<>();
        String sql = " select \n"
                + "               dop.fechavencimiento, \n"
                + "				dop.numerocuota,\n"
                + "               ap.plazo,\n"
                + "               ap.tasainteres,\n"
                + "               ap.fechaoperacion,\n"
                + "               ap.numerooperacion,\n"
                + "               ap.contrato,\n"
                + "			 (dop.montocapital - dop.saldocapital ) as pago\n"
                + "             from ahorroprogramado ap \n"
                + "             left join detalleoperacion dop on dop.idmovimiento=ap.id and dop.tabla='ap'\n"
                + "             left join socio so on so.id=ap.idsocio \n"
                + "             where ap.idestado= 53 \n"
                + "              and dop.idmovimiento = :idmovimiento and\n"
                + "             dop.tabla = 'ap'\n"
                + "             order by ap.fechaoperacion,ap.numerooperacion,dop.numerocuota";
        try {
            Query query = em.createNativeQuery(sql)
                    .setParameter("idmovimiento", idmovimiento);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                AhorroProgramadoDetalle ahorroProgramadoDetalle = new AhorroProgramadoDetalle();
                Date date = new SimpleDateFormat("yyyy-MM-dd").parse(objects[0].toString());
                ahorroProgramadoDetalle.setFechaVencimiento(date);
                ahorroProgramadoDetalle.setNumeroCuota(Integer.parseInt(objects[1].toString()));
                ahorroProgramadoDetalle.setPlazo(Integer.parseInt(objects[2].toString()));
                ahorroProgramadoDetalle.setTasaInteres(Double.parseDouble(objects[3].toString()));
                Date dateOperacion = new SimpleDateFormat("yyyy-MM-dd").parse(objects[4].toString());
                ahorroProgramadoDetalle.setFechaOperacion(dateOperacion);
                ahorroProgramadoDetalle.setNumeroOperacion(Integer.parseInt(objects[5].toString()));
                ahorroProgramadoDetalle.setContrato(objects[6].toString());
                ahorroProgramadoDetalle.setPago(Double.parseDouble(objects[7].toString()));
                lista.add(ahorroProgramadoDetalle);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setData(lista);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("orden-compra-cab/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<OrdenDeCompraCab>> getOrdenCompraCab(@PathParam("cedula") String cedula) {
        Response<List<OrdenDeCompraCab>> response = new Response<>();
        List<OrdenDeCompraCab> lista = new ArrayList<>();
        String sql = " select  m.id, \n"
                + "				m.numerooperacion, \n"
                + "	               m.plazoaprobado, \n"
                + "	               m.numeroboleta, \n"
                + "	               m.fechaaprobado,\n"
                + "	               m.montoaprobado, \n"
                + "	               e.descripcion as entidad, \n"
                + "				 sum(dop.montocapital - dop.saldocapital) as pagado \n"
                + "	             from movimiento m \n"
                + "	             left join detalleoperacion dop on dop.idmovimiento=m.id and dop.tabla='mo' \n"
                + "	             left join socio so on so.id=m.idsocio \n"
                + "	             left join entidad e on e.id = m.identidad \n"
                + "	             where m.idestado=52 and m.idcuenta = 105 \n"
                + "	             and so.cedula= :cedula and m.cancelado = false \n"
                + "	             group by m.id, m.numerosolicitud,m.numerooperacion,m.plazoaprobado, \n"
                + "	             m.tasainteres,m.fechaaprobado,m.montoaprobado, e.descripcion";
        try {
            log.info("### OrdenCompraCab ###");
            log.info("Cedula: " + cedula);
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                OrdenDeCompraCab ordenDeCompraCab = new OrdenDeCompraCab();
                ordenDeCompraCab.setId(Long.parseLong(objects[0].toString()));
                ordenDeCompraCab.setNroOperacion(Integer.parseInt(objects[1].toString()));
                ordenDeCompraCab.setPlazoAprobado(Integer.parseInt(objects[2].toString()));
                ordenDeCompraCab.setNroBoleta(Long.parseLong(objects[3].toString()));
                Date dateOperacion = new SimpleDateFormat("yyyy-MM-dd").parse(objects[4].toString());
                ordenDeCompraCab.setFechaAprobado(dateOperacion);
                ordenDeCompraCab.setMontoAprobado(Double.parseDouble(objects[5].toString()));
                ordenDeCompraCab.setEntidad(objects[6].toString());
                ordenDeCompraCab.setPagado(Double.parseDouble(objects[7].toString()));
                lista.add(ordenDeCompraCab);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setData(lista);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("detalle-movimiento/{idmovimiento}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<DetalleMovimiento>> getDetalleMovimiento(@PathParam("idmovimiento") Long idMovimiento) {
        Response<List<DetalleMovimiento>> response = new Response<>();
        List<DetalleMovimiento> lista = new ArrayList<>();
        String sql = "select \n"
                + "               dop.fechavencimiento, \n"
                + "               dop.numerocuota, \n"
                + "		dop.montocapital + dop.montointeres as cuota, \n"
                + "               (dop.montocapital + dop.montointeres - (dop.saldointeres + dop.saldocapital) ) as pago \n"
                + "               from detalleoperacion dop \n"
                + "               where dop.idmovimiento = :idmovimiento and \n"
                + "               dop.tabla = 'mo'\n"
                + "               order by dop.fechavencimiento,dop.numerocuota";
        try {
            log.info("### DetalleMovimiento ###");
            log.info("IdDetalleMovimiento: " + idMovimiento);
            Query query = em.createNativeQuery(sql)
                    .setParameter("idmovimiento", idMovimiento);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                DetalleMovimiento detalleMovimiento = new DetalleMovimiento();
                Date dateOperacion = new SimpleDateFormat("yyyy-MM-dd").parse(objects[0].toString());
                detalleMovimiento.setFechaVencimiento(dateOperacion);
                detalleMovimiento.setNumeroCuota(Integer.parseInt(objects[1].toString()));
                detalleMovimiento.setCuota(Double.parseDouble(objects[2].toString()));
                detalleMovimiento.setPago(Double.parseDouble(objects[3].toString()));
                lista.add(detalleMovimiento);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("prestamo/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Prestamo>> getPrestamoCab(@PathParam("cedula") String cedula) {
        Response<List<Prestamo>> response = new Response<>();
        List<Prestamo> lista = new ArrayList<>();
        String sql = "	select  m.id, m.numerosolicitud,\n"
                + "				m.numerooperacion,\n"
                + "	               m.plazoaprobado,\n"
                + "	               m.tasainteres,\n"
                + "	               m.fechaaprobado,\n"
                + "	               m.montoaprobado, \n"
                + "	               m.montointeres,\n"
                + "	               e.descripcion as entidad,\n"
                + "	               tc.descripcion as tipoCrecito,\n"
                + "				 sum(dop.montocapital - dop.saldocapital) as pagadocapital,\n"
                + "				 sum(dop.montointeres - dop.saldointeres) as pagadointeres,\n"
                + "				 m.cancelacion,\n"
                + "				 m.montocancelacion,\n"
                + "				 m.montoimpuesto ,\n"
                + "				 m.montosaldo as montoaretirar\n"
                + "	             from movimiento m\n"
                + "	             left join detalleoperacion dop on dop.idmovimiento=m.id and dop.tabla='mo'\n"
                + "	             left join socio so on so.id=m.idsocio \n"
                + "	             left join entidad e on e.id = m.identidad\n"
                + "	             left join tipocredito tc on tc.id = m.idtipocredito\n"
                + "	             where m.idestado=179 and (m.idcuenta = 104 or m.idcuenta = 98)\n"
                + "	             and so.cedula= :cedula and m.cancelado = false \n"
                + "	             group by m.id, m.numerosolicitud,m.numerooperacion,m.plazoaprobado,\n"
                + "	             m.tasainteres,m.fechaaprobado,m.montoaprobado, e.descripcion, tc.descripcion, m.cancelacion,\n"
                + "				 m.montocancelacion,\n"
                + "				 m.montosaldo ,m.montoimpuesto";
        try {
            log.info("### PRESTAMO ###");
            log.info("cedula: " + cedula);
            Query query = em.createNativeQuery(sql)
                    .setParameter("cedula", cedula);
            List<Object[]> list = query.getResultList();
            for (Object[] objects : list) {
                Prestamo prestamo = new Prestamo();
                prestamo.setId(Long.parseLong(objects[0].toString()));
                prestamo.setNumeroSolicitud(Long.parseLong(objects[1].toString()));
                prestamo.setNumeroOperacion(Long.parseLong(objects[2].toString()));
                prestamo.setPlazoAprobado(Integer.parseInt(objects[3].toString()));
                prestamo.setTasaInteres(Double.parseDouble(objects[4].toString()));
                Date dateOperacion = new SimpleDateFormat("yyyy-MM-dd").parse(objects[5].toString());
                prestamo.setFechaAprobado(dateOperacion);
                prestamo.setMontoAprobado(Double.parseDouble(objects[6].toString()));
                prestamo.setMontoInteres(Double.parseDouble(objects[7].toString()));
                prestamo.setEntidad(objects[8].toString());
                prestamo.setTipoCredito(objects[9].toString());
                prestamo.setPagadoCapital(Double.parseDouble(objects[10].toString()));
                prestamo.setPagadoInteres(Double.parseDouble(objects[11].toString()));
                prestamo.setCancelacion(Boolean.valueOf(objects[12].toString()));
                prestamo.setMontoCancelacion(Double.parseDouble(objects[13].toString()));
                prestamo.setMontoImpuesto(Double.parseDouble(objects[14].toString()));
                prestamo.setMontoaRetirar(Double.parseDouble(objects[15].toString()));
                prestamo.setSaldoCapital(prestamo.getMontoAprobado() - prestamo.getPagadoCapital());
                lista.add(prestamo);
            }
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }

    }
}
