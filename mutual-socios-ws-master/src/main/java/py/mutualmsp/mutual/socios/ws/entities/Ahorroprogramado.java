/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "ahorroprogramado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ahorroprogramado.findAll", query = "SELECT a FROM Ahorroprogramado a"),
    @NamedQuery(name = "Ahorroprogramado.findById", query = "SELECT a FROM Ahorroprogramado a WHERE a.id = :id"),
    @NamedQuery(name = "Ahorroprogramado.findByIdsocio", query = "SELECT a FROM Ahorroprogramado a WHERE a.idsocio = :idsocio"),
    @NamedQuery(name = "Ahorroprogramado.findByContrato", query = "SELECT a FROM Ahorroprogramado a WHERE a.contrato = :contrato"),
    @NamedQuery(name = "Ahorroprogramado.findByIdestado", query = "SELECT a FROM Ahorroprogramado a WHERE a.idestado = :idestado"),
    @NamedQuery(name = "Ahorroprogramado.findByIdplan", query = "SELECT a FROM Ahorroprogramado a WHERE a.idplan = :idplan"),
    @NamedQuery(name = "Ahorroprogramado.findByImporte", query = "SELECT a FROM Ahorroprogramado a WHERE a.importe = :importe"),
    @NamedQuery(name = "Ahorroprogramado.findByIdmoneda", query = "SELECT a FROM Ahorroprogramado a WHERE a.idmoneda = :idmoneda"),
    @NamedQuery(name = "Ahorroprogramado.findByPlazo", query = "SELECT a FROM Ahorroprogramado a WHERE a.plazo = :plazo"),
    @NamedQuery(name = "Ahorroprogramado.findByTasainteres", query = "SELECT a FROM Ahorroprogramado a WHERE a.tasainteres = :tasainteres"),
    @NamedQuery(name = "Ahorroprogramado.findByFechaoperacion", query = "SELECT a FROM Ahorroprogramado a WHERE a.fechaoperacion = :fechaoperacion"),
    @NamedQuery(name = "Ahorroprogramado.findByIdpromotor", query = "SELECT a FROM Ahorroprogramado a WHERE a.idpromotor = :idpromotor"),
    @NamedQuery(name = "Ahorroprogramado.findByFechaprimervencimiento", query = "SELECT a FROM Ahorroprogramado a WHERE a.fechaprimervencimiento = :fechaprimervencimiento"),
    @NamedQuery(name = "Ahorroprogramado.findByFechavencimiento", query = "SELECT a FROM Ahorroprogramado a WHERE a.fechavencimiento = :fechavencimiento"),
    @NamedQuery(name = "Ahorroprogramado.findByFechaanulado", query = "SELECT a FROM Ahorroprogramado a WHERE a.fechaanulado = :fechaanulado"),
    @NamedQuery(name = "Ahorroprogramado.findByObservacionanulado", query = "SELECT a FROM Ahorroprogramado a WHERE a.observacionanulado = :observacionanulado"),
    @NamedQuery(name = "Ahorroprogramado.findByFecharetirado", query = "SELECT a FROM Ahorroprogramado a WHERE a.fecharetirado = :fecharetirado"),
    @NamedQuery(name = "Ahorroprogramado.findByObservacionretirado", query = "SELECT a FROM Ahorroprogramado a WHERE a.observacionretirado = :observacionretirado"),
    @NamedQuery(name = "Ahorroprogramado.findByFechacumplido", query = "SELECT a FROM Ahorroprogramado a WHERE a.fechacumplido = :fechacumplido"),
    @NamedQuery(name = "Ahorroprogramado.findByObservacioncumplido", query = "SELECT a FROM Ahorroprogramado a WHERE a.observacioncumplido = :observacioncumplido"),
    @NamedQuery(name = "Ahorroprogramado.findByIdregional", query = "SELECT a FROM Ahorroprogramado a WHERE a.idregional = :idregional"),
    @NamedQuery(name = "Ahorroprogramado.findByNumerooperacion", query = "SELECT a FROM Ahorroprogramado a WHERE a.numerooperacion = :numerooperacion"),
    @NamedQuery(name = "Ahorroprogramado.findByIdcuenta", query = "SELECT a FROM Ahorroprogramado a WHERE a.idcuenta = :idcuenta"),
    @NamedQuery(name = "Ahorroprogramado.findByMontoaportado", query = "SELECT a FROM Ahorroprogramado a WHERE a.montoaportado = :montoaportado"),
    @NamedQuery(name = "Ahorroprogramado.findByMontoliquidacion", query = "SELECT a FROM Ahorroprogramado a WHERE a.montoliquidacion = :montoliquidacion"),
    @NamedQuery(name = "Ahorroprogramado.findByMontointeres", query = "SELECT a FROM Ahorroprogramado a WHERE a.montointeres = :montointeres"),
    @NamedQuery(name = "Ahorroprogramado.findByGastoadministrativo", query = "SELECT a FROM Ahorroprogramado a WHERE a.gastoadministrativo = :gastoadministrativo"),
    @NamedQuery(name = "Ahorroprogramado.findByMontoretirado", query = "SELECT a FROM Ahorroprogramado a WHERE a.montoretirado = :montoretirado"),
    @NamedQuery(name = "Ahorroprogramado.findByTasainteresretirado", query = "SELECT a FROM Ahorroprogramado a WHERE a.tasainteresretirado = :tasainteresretirado")})
public class Ahorroprogramado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsocio")
    private long idsocio;
    @Size(max = 10)
    @Column(name = "contrato")
    private String contrato;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestado")
    private long idestado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idplan")
    private long idplan;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "importe")
    private BigDecimal importe;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmoneda")
    private long idmoneda;
    @Column(name = "plazo")
    private Short plazo;
    @Column(name = "tasainteres")
    private BigDecimal tasainteres;
    @Column(name = "fechaoperacion")
    @Temporal(TemporalType.DATE)
    private Date fechaoperacion;
    @Column(name = "idpromotor")
    private Long idpromotor;
    @Column(name = "fechaprimervencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaprimervencimiento;
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechavencimiento;
    @Column(name = "fechaanulado")
    @Temporal(TemporalType.DATE)
    private Date fechaanulado;
    @Size(max = 100)
    @Column(name = "observacionanulado")
    private String observacionanulado;
    @Column(name = "fecharetirado")
    @Temporal(TemporalType.DATE)
    private Date fecharetirado;
    @Size(max = 100)
    @Column(name = "observacionretirado")
    private String observacionretirado;
    @Column(name = "fechacumplido")
    @Temporal(TemporalType.DATE)
    private Date fechacumplido;
    @Size(max = 100)
    @Column(name = "observacioncumplido")
    private String observacioncumplido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idregional")
    private long idregional;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerooperacion")
    private long numerooperacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuenta")
    private long idcuenta;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoaportado")
    private BigDecimal montoaportado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoliquidacion")
    private BigDecimal montoliquidacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montointeres")
    private BigDecimal montointeres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "gastoadministrativo")
    private BigDecimal gastoadministrativo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montoretirado")
    private BigDecimal montoretirado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "tasainteresretirado")
    private BigDecimal tasainteresretirado;

    public Ahorroprogramado() {
    }

    public Ahorroprogramado(Long id) {
        this.id = id;
    }

    public Ahorroprogramado(Long id, long idsocio, long idestado, long idplan, long idmoneda, long idregional, long numerooperacion, long idcuenta, BigDecimal montoaportado, BigDecimal montoliquidacion, BigDecimal montointeres, BigDecimal gastoadministrativo, BigDecimal montoretirado, BigDecimal tasainteresretirado) {
        this.id = id;
        this.idsocio = idsocio;
        this.idestado = idestado;
        this.idplan = idplan;
        this.idmoneda = idmoneda;
        this.idregional = idregional;
        this.numerooperacion = numerooperacion;
        this.idcuenta = idcuenta;
        this.montoaportado = montoaportado;
        this.montoliquidacion = montoliquidacion;
        this.montointeres = montointeres;
        this.gastoadministrativo = gastoadministrativo;
        this.montoretirado = montoretirado;
        this.tasainteresretirado = tasainteresretirado;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(long idsocio) {
        this.idsocio = idsocio;
    }

    public String getContrato() {
        return contrato;
    }

    public void setContrato(String contrato) {
        this.contrato = contrato;
    }

    public long getIdestado() {
        return idestado;
    }

    public void setIdestado(long idestado) {
        this.idestado = idestado;
    }

    public long getIdplan() {
        return idplan;
    }

    public void setIdplan(long idplan) {
        this.idplan = idplan;
    }

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public long getIdmoneda() {
        return idmoneda;
    }

    public void setIdmoneda(long idmoneda) {
        this.idmoneda = idmoneda;
    }

    public Short getPlazo() {
        return plazo;
    }

    public void setPlazo(Short plazo) {
        this.plazo = plazo;
    }

    public BigDecimal getTasainteres() {
        return tasainteres;
    }

    public void setTasainteres(BigDecimal tasainteres) {
        this.tasainteres = tasainteres;
    }

    public Date getFechaoperacion() {
        return fechaoperacion;
    }

    public void setFechaoperacion(Date fechaoperacion) {
        this.fechaoperacion = fechaoperacion;
    }

    public Long getIdpromotor() {
        return idpromotor;
    }

    public void setIdpromotor(Long idpromotor) {
        this.idpromotor = idpromotor;
    }

    public Date getFechaprimervencimiento() {
        return fechaprimervencimiento;
    }

    public void setFechaprimervencimiento(Date fechaprimervencimiento) {
        this.fechaprimervencimiento = fechaprimervencimiento;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public Date getFechaanulado() {
        return fechaanulado;
    }

    public void setFechaanulado(Date fechaanulado) {
        this.fechaanulado = fechaanulado;
    }

    public String getObservacionanulado() {
        return observacionanulado;
    }

    public void setObservacionanulado(String observacionanulado) {
        this.observacionanulado = observacionanulado;
    }

    public Date getFecharetirado() {
        return fecharetirado;
    }

    public void setFecharetirado(Date fecharetirado) {
        this.fecharetirado = fecharetirado;
    }

    public String getObservacionretirado() {
        return observacionretirado;
    }

    public void setObservacionretirado(String observacionretirado) {
        this.observacionretirado = observacionretirado;
    }

    public Date getFechacumplido() {
        return fechacumplido;
    }

    public void setFechacumplido(Date fechacumplido) {
        this.fechacumplido = fechacumplido;
    }

    public String getObservacioncumplido() {
        return observacioncumplido;
    }

    public void setObservacioncumplido(String observacioncumplido) {
        this.observacioncumplido = observacioncumplido;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }

    public long getNumerooperacion() {
        return numerooperacion;
    }

    public void setNumerooperacion(long numerooperacion) {
        this.numerooperacion = numerooperacion;
    }

    public long getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(long idcuenta) {
        this.idcuenta = idcuenta;
    }

    public BigDecimal getMontoaportado() {
        return montoaportado;
    }

    public void setMontoaportado(BigDecimal montoaportado) {
        this.montoaportado = montoaportado;
    }

    public BigDecimal getMontoliquidacion() {
        return montoliquidacion;
    }

    public void setMontoliquidacion(BigDecimal montoliquidacion) {
        this.montoliquidacion = montoliquidacion;
    }

    public BigDecimal getMontointeres() {
        return montointeres;
    }

    public void setMontointeres(BigDecimal montointeres) {
        this.montointeres = montointeres;
    }

    public BigDecimal getGastoadministrativo() {
        return gastoadministrativo;
    }

    public void setGastoadministrativo(BigDecimal gastoadministrativo) {
        this.gastoadministrativo = gastoadministrativo;
    }

    public BigDecimal getMontoretirado() {
        return montoretirado;
    }

    public void setMontoretirado(BigDecimal montoretirado) {
        this.montoretirado = montoretirado;
    }

    public BigDecimal getTasainteresretirado() {
        return tasainteresretirado;
    }

    public void setTasainteresretirado(BigDecimal tasainteresretirado) {
        this.tasainteresretirado = tasainteresretirado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ahorroprogramado)) {
            return false;
        }
        Ahorroprogramado other = (Ahorroprogramado) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Ahorroprogramado[ id=" + id + " ]";
    }
    
}
