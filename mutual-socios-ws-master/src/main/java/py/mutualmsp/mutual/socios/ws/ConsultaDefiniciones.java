/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.entities.Barrio;
import py.mutualmsp.mutual.socios.ws.entities.Cargo;
import py.mutualmsp.mutual.socios.ws.entities.Ciudad;
import py.mutualmsp.mutual.socios.ws.entities.Departamento;
import py.mutualmsp.mutual.socios.ws.entities.Destino;
import py.mutualmsp.mutual.socios.ws.entities.Institucion;
import py.mutualmsp.mutual.socios.ws.entities.MotivoSolicitudAyuda;
import py.mutualmsp.mutual.socios.ws.entities.NecesidadAyuda;
import py.mutualmsp.mutual.socios.ws.entities.Profesion;
import py.mutualmsp.mutual.socios.ws.entities.Regional;
import py.mutualmsp.mutual.socios.ws.entities.Representante;
import py.mutualmsp.mutual.socios.ws.entities.Tipocredito;

/**
 *
 * @author hectorvillalba
 */
@Stateless
@Path("consulta-definiciones")
public class ConsultaDefiniciones implements Serializable {

    @PersistenceContext
    EntityManager em;
    Logger log = Logger.getLogger("ConsultaDefiniciones");

    @GET
    @Path("getBarrios")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Barrio>> getBarrios() {
        Response<List<Barrio>> response = new Response<>();
        try {
            log.info("### GetBarrios ###");
            List<Barrio> lista = em.createQuery("select b from Barrio b").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }
    
    @GET
    @Path("getDestino")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Destino>> getDestino() {
        Response<List<Destino>> response = new Response<>();
        try {
            log.info("### GetDestino ###");
            List<Destino> lista = em.createQuery("select b from Destino b").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getTipoCredito")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Tipocredito>> getTipoCredito() {
        Response<List<Tipocredito>> response = new Response<>();
        try {
            log.info("### getTipoCredito ###");
            List<Tipocredito> lista = em.createQuery("select b from Tipocredito b where b.id = " + 1).getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getCiudades")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Ciudad>> getCiudades() {
        Response<List<Ciudad>> response = new Response<>();
        try {
            log.info("### GetCiudades ###");
            List<Ciudad> lista = em.createQuery("select b from Ciudad b").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getInstitucion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Institucion>> getInstitucion() {
        Response<List<Institucion>> response = new Response<>();
        try {
            log.info("### GetInstitucion ###");
            List<Institucion> lista = em.createQuery("select b from Institucion b ORDER BY b.descripcion asc").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getProfesion")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Profesion>> getProfesion() {
        Response<List<Profesion>> response = new Response<>();
        try {
            log.info("### GetProfesion ###");
            List<Profesion> lista = em.createQuery("select p from Profesion p ").getResultList();
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getCargo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Cargo>> getCargo() {
        Response<List<Cargo>> response = new Response<>();
        try {
            log.info("### GetCargo###");
            List<Cargo> lista = em.createQuery("select b from Cargo b order by b.descripcion asc").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getDepartamento")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Departamento>> getDepartamento() {
        Response<List<Departamento>> response = new Response<>();
        try {
            log.info("### Get Departamento###");
            List<Departamento> lista = em.createQuery("select b from Departamento b").getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getCiudadByDepartamento/{iddep}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Ciudad>> getCiudadByDepartamento(@PathParam("iddep") Long iddep) {
        Response<List<Ciudad>> response = new Response<>();
        try {
            log.info("### Get Ciudad by Departamento###");
            List<Ciudad> lista = em.createQuery("select b from Ciudad b where b.iddepartamento = " + iddep).getResultList();
            log.info("Size: " + lista.size());
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(200);
            response.setMensaje("Error: " + e.getMessage());
            e.printStackTrace();
            return response;
        }
    }

    @GET
    @Path("getMotivoAyuda")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<MotivoSolicitudAyuda>> getMotivoAyuda() {
        Response<List<MotivoSolicitudAyuda>> response = new Response<>();
        try {
            log.info("### getMotivoAyuda ###");
            List<MotivoSolicitudAyuda> lista = em.createQuery("select m from MotivoSolicitudAyuda m order by m.id asc ").getResultList();
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setData(null);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("getNecesidadAyuda")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<NecesidadAyuda>> getNecesidadAyuda() {
        Response<List<NecesidadAyuda>> response = new Response<>();
        try {
            log.info("### getNecesidadAyuda ###");
            List<NecesidadAyuda> lista = em.createQuery("select m from NecesidadAyuda m order by m.id asc ").getResultList();
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setData(null);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }
    
    @GET
    @Path("getRepresentante")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Representante>> getRepresentante() {
        Response<List<Representante>> response = new Response<>();
        try {
            log.info("### getRepresentante ###");
            List<Representante> lista = em.createQuery("select m from Representante m ORDER BY m.nombre, m.apellido asc").getResultList();
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setData(null);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }
    
    @GET
    @Path("getRegional")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<Regional>> getRegional() {
        Response<List<Regional>> response = new Response<>();
        try {
            log.info("### getRegional ###");
            List<Regional> lista = em.createQuery("select m from Regional m ORDER BY m.descripcion asc").getResultList();
            response.setCodigo(200);
            response.setData(lista);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            response.setCodigo(400);
            response.setData(null);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

}
