/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "operacionexterna")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Operacionexterna.findAll", query = "SELECT o FROM Operacionexterna o"),
    @NamedQuery(name = "Operacionexterna.findById", query = "SELECT o FROM Operacionexterna o WHERE o.id = :id"),
    @NamedQuery(name = "Operacionexterna.findByIdsocio", query = "SELECT o FROM Operacionexterna o WHERE o.idsocio = :idsocio"),
    @NamedQuery(name = "Operacionexterna.findByCedula", query = "SELECT o FROM Operacionexterna o WHERE o.cedula = :cedula"),
    @NamedQuery(name = "Operacionexterna.findByIdcuenta", query = "SELECT o FROM Operacionexterna o WHERE o.idcuenta = :idcuenta"),
    @NamedQuery(name = "Operacionexterna.findByBoleta", query = "SELECT o FROM Operacionexterna o WHERE o.boleta = :boleta"),
    @NamedQuery(name = "Operacionexterna.findByFecha", query = "SELECT o FROM Operacionexterna o WHERE o.fecha = :fecha"),
    @NamedQuery(name = "Operacionexterna.findByMonto", query = "SELECT o FROM Operacionexterna o WHERE o.monto = :monto"),
    @NamedQuery(name = "Operacionexterna.findByPlazo", query = "SELECT o FROM Operacionexterna o WHERE o.plazo = :plazo"),
    @NamedQuery(name = "Operacionexterna.findByCuota", query = "SELECT o FROM Operacionexterna o WHERE o.cuota = :cuota"),
    @NamedQuery(name = "Operacionexterna.findBySaldo", query = "SELECT o FROM Operacionexterna o WHERE o.saldo = :saldo"),
    @NamedQuery(name = "Operacionexterna.findByCantidad", query = "SELECT o FROM Operacionexterna o WHERE o.cantidad = :cantidad"),
    @NamedQuery(name = "Operacionexterna.findByNombre", query = "SELECT o FROM Operacionexterna o WHERE o.nombre = :nombre"),
    @NamedQuery(name = "Operacionexterna.findByApellido", query = "SELECT o FROM Operacionexterna o WHERE o.apellido = :apellido")})
public class Operacionexterna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsocio")
    private long idsocio;
    @Size(max = 10)
    @Column(name = "cedula")
    private String cedula;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcuenta")
    private long idcuenta;
    @Size(max = 10)
    @Column(name = "boleta")
    private String boleta;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "monto")
    private BigDecimal monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "plazo")
    private short plazo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cuota")
    private BigDecimal cuota;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldo")
    private BigDecimal saldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad")
    private short cantidad;
    @Size(max = 70)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 70)
    @Column(name = "apellido")
    private String apellido;

    public Operacionexterna() {
    }

    public Operacionexterna(Long id) {
        this.id = id;
    }

    public Operacionexterna(Long id, long idsocio, long idcuenta, BigDecimal monto, short plazo, BigDecimal cuota, BigDecimal saldo, short cantidad) {
        this.id = id;
        this.idsocio = idsocio;
        this.idcuenta = idcuenta;
        this.monto = monto;
        this.plazo = plazo;
        this.cuota = cuota;
        this.saldo = saldo;
        this.cantidad = cantidad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(long idsocio) {
        this.idsocio = idsocio;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public long getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(long idcuenta) {
        this.idcuenta = idcuenta;
    }

    public String getBoleta() {
        return boleta;
    }

    public void setBoleta(String boleta) {
        this.boleta = boleta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }

    public short getPlazo() {
        return plazo;
    }

    public void setPlazo(short plazo) {
        this.plazo = plazo;
    }

    public BigDecimal getCuota() {
        return cuota;
    }

    public void setCuota(BigDecimal cuota) {
        this.cuota = cuota;
    }

    public BigDecimal getSaldo() {
        return saldo;
    }

    public void setSaldo(BigDecimal saldo) {
        this.saldo = saldo;
    }

    public short getCantidad() {
        return cantidad;
    }

    public void setCantidad(short cantidad) {
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Operacionexterna)) {
            return false;
        }
        Operacionexterna other = (Operacionexterna) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Operacionexterna[ id=" + id + " ]";
    }
    
}
