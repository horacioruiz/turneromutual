/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import py.mutualmsp.mutual.socios.ws.dto.UsuarioDto;
import py.mutualmsp.mutual.socios.ws.entities.Usuario;

/**
 *
 * @author hectorvillalba
 */

@Stateless
@Path("funcionarios")
public class FuncionariosWS {
    
    @PersistenceContext 
    EntityManager em;
    Logger log = Logger.getLogger("FuncionariosWS");
    
    @POST
    @Path("login")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Usuario login(UsuarioDto usuarioDto){
        Usuario usuario = null;
        try {
            log.info("### Login Funcionarios ###");
            usuario = (Usuario) em.createQuery("select u from Usuario u where u.usuario = :usuario and u.contrasena = :contra ")
                    .setParameter("usuario", usuarioDto.getUsuario())
                    .setParameter("contra", usuarioDto.getContrasena())
                    .getSingleResult();
            log.info("Usuario encontrado: " + usuario.getUsuario());
        } catch (NoResultException e) {
            log.info("No se encontre funcionario...!!!");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuario;
    }
    
}
