/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "entidad")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Entidad.findAll", query = "SELECT e FROM Entidad e"),
    @NamedQuery(name = "Entidad.findById", query = "SELECT e FROM Entidad e WHERE e.id = :id"),
    @NamedQuery(name = "Entidad.findByDescripcion", query = "SELECT e FROM Entidad e WHERE e.descripcion = :descripcion"),
    @NamedQuery(name = "Entidad.findByDireccion", query = "SELECT e FROM Entidad e WHERE e.direccion = :direccion"),
    @NamedQuery(name = "Entidad.findByTelefono", query = "SELECT e FROM Entidad e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Entidad.findByIdtipoentidad", query = "SELECT e FROM Entidad e WHERE e.idtipoentidad = :idtipoentidad"),
    @NamedQuery(name = "Entidad.findByRuc", query = "SELECT e FROM Entidad e WHERE e.ruc = :ruc"),
    @NamedQuery(name = "Entidad.findByIdciudad", query = "SELECT e FROM Entidad e WHERE e.idciudad = :idciudad"),
    @NamedQuery(name = "Entidad.findByContacto", query = "SELECT e FROM Entidad e WHERE e.contacto = :contacto"),
    @NamedQuery(name = "Entidad.findByWeb", query = "SELECT e FROM Entidad e WHERE e.web = :web"),
    @NamedQuery(name = "Entidad.findByEmail", query = "SELECT e FROM Entidad e WHERE e.email = :email"),
    @NamedQuery(name = "Entidad.findByIdramo", query = "SELECT e FROM Entidad e WHERE e.idramo = :idramo"),
    @NamedQuery(name = "Entidad.findByPlazomaximo", query = "SELECT e FROM Entidad e WHERE e.plazomaximo = :plazomaximo"),
    @NamedQuery(name = "Entidad.findByBonificacion", query = "SELECT e FROM Entidad e WHERE e.bonificacion = :bonificacion"),
    @NamedQuery(name = "Entidad.findByTasainteres", query = "SELECT e FROM Entidad e WHERE e.tasainteres = :tasainteres"),
    @NamedQuery(name = "Entidad.findByActivo", query = "SELECT e FROM Entidad e WHERE e.activo = :activo"),
    @NamedQuery(name = "Entidad.findByPromotorautorizado", query = "SELECT e FROM Entidad e WHERE e.promotorautorizado = :promotorautorizado"),
    @NamedQuery(name = "Entidad.findByIdregional", query = "SELECT e FROM Entidad e WHERE e.idregional = :idregional"),
    @NamedQuery(name = "Entidad.findByCtaActiva", query = "SELECT e FROM Entidad e WHERE e.ctaActiva = :ctaActiva"),
    @NamedQuery(name = "Entidad.findByCtaPasiva", query = "SELECT e FROM Entidad e WHERE e.ctaPasiva = :ctaPasiva")})
public class Entidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 50)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 40)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipoentidad")
    private long idtipoentidad;
    @Size(max = 12)
    @Column(name = "ruc")
    private String ruc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idciudad")
    private long idciudad;
    @Size(max = 50)
    @Column(name = "contacto")
    private String contacto;
    @Size(max = 30)
    @Column(name = "web")
    private String web;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idramo")
    private long idramo;
    @Column(name = "plazomaximo")
    private Short plazomaximo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "bonificacion")
    private BigDecimal bonificacion;
    @Column(name = "tasainteres")
    private BigDecimal tasainteres;
    @Column(name = "activo")
    private Boolean activo;
    @Size(max = 100)
    @Column(name = "promotorautorizado")
    private String promotorautorizado;
    @Column(name = "idregional")
    private Long idregional;
    @Size(max = 100)
    @Column(name = "cta_activa")
    private String ctaActiva;
    @Size(max = 100)
    @Column(name = "cta_pasiva")
    private String ctaPasiva;

    public Entidad() {
    }

    public Entidad(Long id) {
        this.id = id;
    }

    public Entidad(Long id, String descripcion, long idtipoentidad, long idciudad, long idramo) {
        this.id = id;
        this.descripcion = descripcion;
        this.idtipoentidad = idtipoentidad;
        this.idciudad = idciudad;
        this.idramo = idramo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public long getIdtipoentidad() {
        return idtipoentidad;
    }

    public void setIdtipoentidad(long idtipoentidad) {
        this.idtipoentidad = idtipoentidad;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public long getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(long idciudad) {
        this.idciudad = idciudad;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getWeb() {
        return web;
    }

    public void setWeb(String web) {
        this.web = web;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getIdramo() {
        return idramo;
    }

    public void setIdramo(long idramo) {
        this.idramo = idramo;
    }

    public Short getPlazomaximo() {
        return plazomaximo;
    }

    public void setPlazomaximo(Short plazomaximo) {
        this.plazomaximo = plazomaximo;
    }

    public BigDecimal getBonificacion() {
        return bonificacion;
    }

    public void setBonificacion(BigDecimal bonificacion) {
        this.bonificacion = bonificacion;
    }

    public BigDecimal getTasainteres() {
        return tasainteres;
    }

    public void setTasainteres(BigDecimal tasainteres) {
        this.tasainteres = tasainteres;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getPromotorautorizado() {
        return promotorautorizado;
    }

    public void setPromotorautorizado(String promotorautorizado) {
        this.promotorautorizado = promotorautorizado;
    }

    public Long getIdregional() {
        return idregional;
    }

    public void setIdregional(Long idregional) {
        this.idregional = idregional;
    }

    public String getCtaActiva() {
        return ctaActiva;
    }

    public void setCtaActiva(String ctaActiva) {
        this.ctaActiva = ctaActiva;
    }

    public String getCtaPasiva() {
        return ctaPasiva;
    }

    public void setCtaPasiva(String ctaPasiva) {
        this.ctaPasiva = ctaPasiva;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Entidad)) {
            return false;
        }
        Entidad other = (Entidad) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Entidad[ id=" + id + " ]";
    }
    
}
