/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class Prestamo implements Serializable{

    @Getter
    @Setter    
    private Long id;
    
    @Getter
    @Setter
    private Long numeroSolicitud;
    
    @Getter
    @Setter
    private Long numeroOperacion;
    
    @Getter
    @Setter
    private Integer plazoAprobado;
    
    @Getter
    @Setter
    private Double tasaInteres;
    
    @Getter
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Asuncion")
    private Date fechaAprobado;
    
    @Getter
    @Setter
    private Double montoAprobado;
    
    @Getter
    @Setter
    private Double montoInteres;
    
    @Getter
    @Setter
    private String entidad;
    
    @Getter
    @Setter
    private String tipoCredito;
    
    @Getter
    @Setter
    private Double pagadoCapital;
    
    @Getter
    @Setter
    private Double pagadoInteres;
    
    @Getter
    @Setter
    private Boolean cancelacion;
    
    @Getter
    @Setter 
    private Double montoCancelacion;
    
    @Getter
    @Setter
    private Double montoImpuesto;
    
    @Getter
    @Setter
    private Double montoaRetirar;
    
    @Getter
    @Setter
    private Double saldoCapital;
    
    
    
    
}
