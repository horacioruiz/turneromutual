/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.entities.DatosFamiliares;
import py.mutualmsp.mutual.socios.ws.entities.DatosSocioDemograficos;
import py.mutualmsp.mutual.socios.ws.entities.Socio;
import py.mutualmsp.mutual.socios.ws.entities.SolicitudAyuda;
import py.mutualmsp.mutual.socios.ws.entities.Usuario;
import py.mutualmsp.mutual.socios.ws.util.Constants;
import py.mutualmsp.mutual.socios.ws.util.DBConnection;
import py.mutualmsp.mutual.socios.ws.util.Util;

/**
 *
 * @author hectorvillalba
 */


@Stateless
@Path("solicitud-ayuda")
public class SolicitudAyudaWS implements Serializable{
    
    @PersistenceContext
    EntityManager em;
    
    
    Logger log = Logger.getLogger(SolicitudAyudaWS.class.getSimpleName());
    
    
    @Path("validar-socio/{cedula}/{fecha}/{user}/{pass}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SolicitudAyuda> getSocio(@PathParam("cedula") String cedula,
                                          @PathParam("fecha")String fecha,
                                          @PathParam("user")String user,
                                          @PathParam("pass")String pass){
        Response<SolicitudAyuda> response = new Response<>();
        try {
            
            log.info("### Validar Socio ###");
            if (user ==null || user.equals("")) {
                response.setMensaje("El usuario no puede estar vacio");
                log.info("El usuario no puede estar vacio");
                response.setCodigo(205);
                response.setData(null);
                return response;                
            }
            if (pass == null || pass.equals("")) {
                response.setMensaje("El password no puede estar vacio");
                log.info("El password no puede estar vacio");
                response.setCodigo(205);
                response.setData(null);
                return response;                 
            }
            Usuario usuario = null;
            Socio socio = null;
            try{
                socio = (Socio) em.createQuery("select s from Socio s where s.cedula = :cedula and s.idestadosocio = 20 ")
                                .setParameter("cedula", cedula)
                                .getSingleResult();                
            }catch(NoResultException e){
                response.setCodigo(205);
                response.setData(null);
                response.setMensaje("No se encontraron datos");
                return response;             
            }   
            
            SolicitudAyuda ayuda = new SolicitudAyuda();
            try {
                 usuario = (Usuario)em.createQuery("select u from Usuario u where u.usuario = :user and u.contrasena = :pass ")
                         .setParameter("user", user)
                         .setParameter("pass", pass)
                         .getSingleResult();
            } catch (NoResultException e) {
                response.setMensaje("User o Password invalido, no se encontro usuario");
                log.info("User o Password invalido, no se encontro usuario");
                response.setCodigo(205);
                response.setData(null);
                return response;                   
            }
            log.info("cedula: " + cedula);
            log.info("fecha: " + fecha);
            log.info("user: " + user);
            log.info("pass: " + pass);
            try {
                ayuda = (SolicitudAyuda) em.createQuery("select u from SolicitudAyuda u where u.idsocio.cedula = :cedula")
                         .setParameter("cedula", cedula)
                         .getSingleResult();
                response.setCodigo(205);
                response.setData(ayuda);
                response.setMensaje("Ya se envio una solicitud");
                log.info("Se encontro solicitud");
                return response;
            } catch (NoResultException e) {
                log.info("Crearemos una solicitud");
            } catch (Exception e) {
                e.printStackTrace();
            }
              
            
            String fechaNacimiento = new SimpleDateFormat("ddMMyyyy").format(socio.getFechanacimiento());
            log.info("fechaNacimiento: " + fechaNacimiento);
            if (!fecha.equals(fechaNacimiento)) {
                response.setCodigo(206);
                response.setData(null);
                response.setMensaje("No coincide los datos");
                return response;
            }
            ayuda.setIdsocio(socio);
            response.setCodigo(200);
            response.setData(ayuda);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(208);
            response.setData(null);
            response.setMensaje("No coinciden los datos");
            return response;
        }
    }
    
    @POST
    @Path("uploadImage/{id}/{nrofoto}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response<SolicitudAyuda> upLoadImage(@PathParam("id") Long id, 
                                                @PathParam("nrofoto")Integer nroFoto,
                                                MultipartFormDataInput input) {
        Response<SolicitudAyuda> response = new Response<>();
        try {
            log.info("### UpLoadImage ###");
              log.info("ID: " + id);
            
            SolicitudAyuda marcacion = (SolicitudAyuda) em.createQuery("select m from SolicitudAyuda m where m.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
                    // Guardamos el archivo en disco
            Map<String, String> map = Util.saveFileToDiskImage(
                    input,
                    Constants.UPLOAD_DIR + "/solicitudAyuda/" + marcacion.getIdsocio().getCedula()+ "/",
                    id.toString(),
                    nroFoto
            );
            
            if (nroFoto ==1) {
                log.info("nroFoto: " + nroFoto);
                marcacion.setUrlFoto1(Constants.UPLOAD_DIR_IMG + "\\\\" + marcacion.getIdsocio().getCedula() + "\\\\" + map.get("fileName").toString());
            }
            if(nroFoto ==2){
                log.info("nroFoto: " + nroFoto);
                marcacion.setUrlFoto2(Constants.UPLOAD_DIR_IMG + "\\\\" + marcacion.getIdsocio().getCedula() + "\\\\" + map.get("fileName").toString());
            }
            if(nroFoto ==3){
                log.info("nroFoto: " + nroFoto);
                marcacion.setUrlFoto3(Constants.UPLOAD_DIR_IMG + "\\\\" + marcacion.getIdsocio().getCedula() + "\\\\" + map.get("fileName").toString());
            }
            if(nroFoto ==4){
                marcacion.setUrlLiquidacion(Constants.UPLOAD_DIR_IMG + "\\\\" + marcacion.getIdsocio().getCedula() + "\\\\" + map.get("fileName").toString());
            }
            em.merge(marcacion);
            em.flush();
                //archivoDet.setUrl(Constants.PUBLIC_SERVER_URL + tipoArchivoEnum.getUploadDir() + fileName);
            response.setCodigo(200);
            response.setData(marcacion);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }
    
    @Path("registrar/{user}/{pass}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response<SolicitudAyuda> registrarSolicitud(@PathParam("user") String user,
                                             @PathParam("pass") String pass, 
                                             SolicitudAyuda solicitudAyuda){
        Response<SolicitudAyuda> response = new Response<>();
        try {
            log.info("### Registrar Solicitudad Ayuda ###");
            if (user ==null || user.equals("")) {
                response.setMensaje("El usuario no puede estar vacio");
                log.info("El usuario no puede estar vacio");
                response.setCodigo(205);
                response.setData(null);
                return response;                
            }
            if (pass == null || pass.equals("")) {
                response.setMensaje("El password no puede estar vacio");
                log.info("El password no puede estar vacio");
                response.setCodigo(205);
                response.setData(null);
                return response;                 
            }
            Usuario usuario = null;
            try {
                 usuario = (Usuario)em.createQuery("select u from Usuario u where u.usuario = :user and u.contrasena = :pass ")
                         .setParameter("user", user)
                         .setParameter("pass", pass)
                         .getSingleResult();
            } catch (NoResultException e) {
                response.setMensaje("User o Password invalido, no se encontro usuario");
                log.info("User o Password invalido, no se encontro usuario");
                response.setCodigo(205);
                response.setData(null);
                return response;                   
            }
            try {
                 SolicitudAyuda ayuda = (SolicitudAyuda)em.createQuery("select u from SolicitudAyuda u where u.idsocio.cedula = :cedula")
                         .setParameter("cedula", solicitudAyuda.getIdsocio().getCedula())
                         .getSingleResult();
                 if (ayuda!=null) {
                    response.setMensaje("ya existe una solicitud cargada");
                    log.info("ya existe una solicitud cargada");
                    response.setCodigo(205);
                    response.setData(ayuda);
                    return response;                     
                }
            } catch (NoResultException e) {
                log.info("Es una solicitud nueva...!!!");                 
            }

            
            log.info("Nombre: " + solicitudAyuda.getIdsocio().getNombre());
            log.info("Apellido: " + solicitudAyuda.getIdsocio().getApellido());
            log.info("Ciudad: " + solicitudAyuda.getIdciudad().getDescripcion());
            log.info("Departamento: " + solicitudAyuda.getIddepartamento().getDescripcion());
            log.info("Institución: " + solicitudAyuda.getIddepartamento().getDescripcion());
            log.info("Cargo: " + solicitudAyuda.getCargo());
            solicitudAyuda.setFecha(new Date());
            solicitudAyuda.setEstado("PENDIENTE");
            
            em.persist(solicitudAyuda);
            em.flush();
            
            if (solicitudAyuda.getDatosFamiliares()!= null) {
                if(!solicitudAyuda.getDatosFamiliares().isEmpty()){
                    for (DatosFamiliares object : solicitudAyuda.getDatosFamiliares()) {
                        object.setSolicitudAyuda(solicitudAyuda);
                        em.merge(object);
                    } 
                }
            }
           
            if (solicitudAyuda.getDatosSociosDemograficos()!= null) {
                if(!solicitudAyuda.getDatosSociosDemograficos().isEmpty()){
                    for (DatosSocioDemograficos object : solicitudAyuda.getDatosSociosDemograficos()) {
                        object.setSolicitudAyuda(solicitudAyuda);
                        em.merge(object);
                    } 
                }
            }
            //Enviamos sms
            enviarSMS(solicitudAyuda.getIdsocio(), 
                    solicitudAyuda.getTelefono(), 
                    solicitudAyuda.getId(),
                    usuario.getIdfuncionario());
            
            response.setCodigo(200);
            response.setData(solicitudAyuda);
            response.setMensaje("Proceso satisfactorio");
            log.info("Proceso satisfactorio");
            return response;
        } catch (Exception e) { 
            log.error("Ocurrio un error: " + e.getMessage());
            e.printStackTrace();
            response.setCodigo(205);
            response.setData(null);
            response.setMensaje("Ocurrio un error al procesar los datos...!!!");
            return response;
        }
    }

    private void enviarSMS(Socio socio, String telefono, Long id, Long idFuncionario) throws ClassNotFoundException, SQLException {
        DBConnection dBConnection = new DBConnection();
        String res="";
        try {
           Statement statement = dBConnection.getConnection().createStatement();
           String sql = "SELECT sms.programar_sms('"+telefono+"'," +
            "'Estimado " +socio.getNombre() + " " + socio.getApellido() +" su solicitud ha sido procesada con exito su Nro. de Referencia es el "+id+" Atte: Mutual Nac. del MSP Y BS'" +","  + socio.getId()+ "," + idFuncionario + ");";
            log.info(sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()){
              res=resultSet.getString(1);
              log.info("programar_sms: " + res);
            }
         }
       catch (SQLException ex) {
            System.err.println( ex.getMessage() );
       }finally{
            dBConnection.getConnection().close();
        }
    }
}
