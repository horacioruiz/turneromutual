/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

/**
 *
 * @author hruiz
 */
public class PersonaDto {

    private Long id;

    private String razonsocial;

    private String cedula;

    private String ruc;

    private String nombre;

    private String apellido;

    private Long idpersoneria;

    private Long idciudad;

    private Long idsocio;

    private Long idplancuenta;

    public PersonaDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRazonsocial() {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial) {
        this.razonsocial = razonsocial;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Long getIdpersoneria() {
        return idpersoneria;
    }

    public void setIdpersoneria(Long idpersoneria) {
        this.idpersoneria = idpersoneria;
    }

    public Long getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Long idciudad) {
        this.idciudad = idciudad;
    }

    public Long getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(Long idsocio) {
        this.idsocio = idsocio;
    }

    public Long getIdplancuenta() {
        return idplancuenta;
    }

    public void setIdplancuenta(Long idplancuenta) {
        this.idplancuenta = idplancuenta;
    }

}
