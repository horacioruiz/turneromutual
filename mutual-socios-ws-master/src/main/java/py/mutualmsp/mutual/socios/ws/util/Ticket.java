/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.util;

import java.util.Locale;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.CancelablePrintJob;
import javax.print.Doc;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.standard.PrinterName;
import javax.print.attribute.standard.QueuedJobCount;

/**
 *
 * @author Hruiz
 */
public class Ticket {

    private static DocPrintJob job;
    public static Timer timer = null;
    // private NumberValidator numValidator;
    public byte[] bytes;
    boolean slip;

    public void print() {
        // this.bytes = this.contentTicket.getBytes();
//        String impresora = recuperarImprtesora();
        String impresora = "EPSON TM-U220 Receipt";
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(bytes, flavor, null);
        AttributeSet attributes = new HashPrintServiceAttributeSet(
                new PrinterName(impresora, Locale.getDefault()));
        PrintService[] services = PrintServiceLookup.lookupPrintServices(
                flavor, attributes);
        job = null;
        if (services.length > 0) {
            for (int i = 0; i < services.length; i++) {
                if (services[i].getName().equals(impresora)) {
                    job = services[i].createPrintJob();
                    // verificandoColaImpresora(services[i]);
                }
            }
        }
        // if (job != null) { ORGINAL, PARA QUE CANCELE LAS DEMAS COLAS DE
        // IMPRESIONES.
        // try {
        // job.print(doc, null);
        // if (!slip) {
        // timer = new Timer();
        // timer.schedule(new PrinterHandler(), 3000, 10000);
        // }
        // } catch (PrintException ex) {
        // cancelPrinting();
        // Utilidades.log.error("ERROR PrintException: ",
        // ex.fillInStackTrace());
        // }
        // }
        if (job != null) {
            try {
                job.print(doc, null);
                // if (!slip) {
                // timer = new Timer();
                // timer.schedule(new PrinterHandler(), 3000, 10000);
                // }
            } catch (PrintException ex) {
                Logger.getLogger(Ticket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

//    private String recuperarImprtesora() {
//        String valor = "";
//        Conexion.conectar();
//        String sql = "SELECT * FROM general.impresora WHERE id_impresora="
//                + Utilidades.idImpresora;
//        System.out.println("-->> " + sql);
//        try (PreparedStatement ps = Conexion.getCon().prepareStatement(sql)) {
//            ResultSet rs = ps.executeQuery();
//            if (rs.next()) {
//                valor = rs.getString("impresora");
//            }
//            ps.close();
//        } catch (SQLException ex) {
//            System.out.println(ex.getLocalizedMessage());
//        }
//        // actualizarRangoCancelFact(valor);
//        Conexion.cerrar();
//        return valor;
//    }
    private static void cancelPrinting() {
        try {
            CancelablePrintJob cancelableJob = (CancelablePrintJob) job;
            cancelableJob.cancel();
        } catch (PrintException ex) {
            System.out
                    .println("ERROR PrintException: " + ex.fillInStackTrace());

        }
    }

    private void verificandoColaImpresora(PrintService printService) {
        if (Integer.valueOf(printService.getAttribute(QueuedJobCount.class)
                .toString()) > 1) {
            cancelPrinting();
        }
    }

}
