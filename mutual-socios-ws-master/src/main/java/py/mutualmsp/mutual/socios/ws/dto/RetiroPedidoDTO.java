package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetiroPedidoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRetiro;

    // private FacturaClienteCabDTO facturaClienteCab;
    private String numero;

    private String cliente;

    private String caja;

    private String serie;

    private boolean entregado;

    private boolean preparacion;

    private boolean listo;

    private boolean llamar;

    private String hora;

    private SociosDto socio;

    private PersonaDto persona;

    private String ci;

    public RetiroPedidoDTO() {
        super();
    }

    public Long getIdRetiro() {
        return idRetiro;
    }

    public void setIdRetiro(Long idRetiro) {
        this.idRetiro = idRetiro;
    }

    // public FacturaClienteCabDTO getFacturaClienteCab() {
    // return facturaClienteCab;
    // }
    public String getSerie() {
        return serie;
    }

    public PersonaDto getPersona() {
        return persona;
    }

    public void setPersona(PersonaDto persona) {
        this.persona = persona;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public boolean isLlamar() {
        return llamar;
    }

    public void setLlamar(boolean llamar) {
        this.llamar = llamar;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getHora() {
        return hora;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public SociosDto getSocio() {
        return socio;
    }

    public void setSocio(SociosDto socio) {
        this.socio = socio;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    // public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab)
    // {
    // this.facturaClienteCab = facturaClienteCab;
    // }
    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public void setEntregado(boolean entregado) {
        this.entregado = entregado;
    }

    public boolean isPreparacion() {
        return preparacion;
    }

    public void setPreparacion(boolean preparacion) {
        this.preparacion = preparacion;
    }

    public boolean isListo() {
        return listo;
    }

    public void setListo(boolean listo) {
        this.listo = listo;
    }

}
