/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class DetalleMovimiento implements Serializable{
    
    @Getter
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Asuncion")
    private Date fechaVencimiento;
    
    @Getter
    @Setter
    private Integer numeroCuota;
    
    @Getter
    @Setter
    private Double cuota;
    
    @Getter
    @Setter
    private Double pago;
    
    
}
