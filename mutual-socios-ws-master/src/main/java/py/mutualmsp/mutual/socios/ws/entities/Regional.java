/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "regional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Regional.findAll", query = "SELECT r FROM Regional r")
    , @NamedQuery(name = "Regional.findById", query = "SELECT r FROM Regional r WHERE r.id = :id")
    , @NamedQuery(name = "Regional.findByDescripcion", query = "SELECT r FROM Regional r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Regional.findByIdzona", query = "SELECT r FROM Regional r WHERE r.idzona = :idzona")
    , @NamedQuery(name = "Regional.findByIdprocedencia", query = "SELECT r FROM Regional r WHERE r.idprocedencia = :idprocedencia")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Regional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idzona")
    private long idzona;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "idprocedencia")
    private BigDecimal idprocedencia;

    public Regional() {
    }

    public Regional(Long id) {
        this.id = id;
    }

    public Regional(Long id, String descripcion, long idzona) {
        this.id = id;
        this.descripcion = descripcion;
        this.idzona = idzona;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getIdzona() {
        return idzona;
    }

    public void setIdzona(long idzona) {
        this.idzona = idzona;
    }

    public BigDecimal getIdprocedencia() {
        return idprocedencia;
    }

    public void setIdprocedencia(BigDecimal idprocedencia) {
        this.idprocedencia = idprocedencia;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Regional)) {
            return false;
        }
        Regional other = (Regional) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Regional[ id=" + id + " ]";
    }
    
}
