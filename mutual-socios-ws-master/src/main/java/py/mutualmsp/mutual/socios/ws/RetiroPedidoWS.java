/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import py.mutualmsp.mutual.socios.ws.dto.PersonaDto;
import py.mutualmsp.mutual.socios.ws.dto.RetiroPedidoDTO;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.dto.SociosDto;
import py.mutualmsp.mutual.socios.ws.entities.AnoPeriodo;
import py.mutualmsp.mutual.socios.ws.entities.Funcionario;
import py.mutualmsp.mutual.socios.ws.entities.Persona;
import py.mutualmsp.mutual.socios.ws.entities.RetiroPedido;
import py.mutualmsp.mutual.socios.ws.entities.Socio;
import py.mutualmsp.mutual.socios.ws.util.ComandosImpresoraEpson;
import py.mutualmsp.mutual.socios.ws.util.Ticket;

/**
 *
 * @author Hruiz
 */
@Stateless
@Path("retiroPedido")
public class RetiroPedidoWS implements Serializable {

    @PersistenceContext
    private EntityManager em;
    Logger log = Logger.getLogger(RetiroPedidoWS.class.getSimpleName());
    ComandosImpresoraEpson cie = new ComandosImpresoraEpson();

    @GET
    @Path("imprimirTicketAnterior/{descri}/{numero}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> getImpresionAnterior(@PathParam("descri") String descri,
            @PathParam("numero") String numero) {
        Response<String> response = new Response<>();
        String dStr = "<center>" + descri + "</center>";
        dStr += "<center>&nbsp;</center>";
        dStr += "<center>" + numero + "</center>";
        dStr += "<center>&nbsp;</center>";
        dStr += "<center>La Mutual de Funcionarios del MSP Y BS</center>";
        try {
            // System.out.println("TICKET 00 " + dStr);
            dStr = dStr.replaceAll("<center>", "");
            dStr = dStr.replaceAll("&&", "/");
            dStr = dStr.replaceAll("</center>", "\n");
            dStr = dStr.replaceAll("<center style='color: red'>", "");
            dStr = dStr.replaceAll("<br>", "\n");
            dStr = dStr.replaceAll("&nbsp;", " ");
            System.out.println("TICKET 01 " + dStr);
            System.out.println("*********************OPA********************");
            dStr = (char) 0 + (char) 27 + "a" + (char) 1
                    + (char) 27 + "R" + (char) 7 + (char) 27 + "c0" + (char) 3
                    + (char) 27 + "z" + (char) 1 + (char) 13 + dStr;
//                    + 
//                    +(char) 27 + (char) 100 + (char) 13 + (char) 27
//                    + (char) 105 + (char) 0;
//            dStr+="";
            dStr += new char[]{0x1B, 'm'};
            System.out.println("TICKET 02 " + dStr);
            Ticket tic = new Ticket();
            tic.bytes = dStr.getBytes();
            tic.print();
            response.setCodigo(200);
            response.setData("OK");
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        } finally {
        }
    }

    @GET
    @Path("imprimirTicket/{descri}/{numero}/{nombre}/{cedula}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<String> getImpresion(@PathParam("descri") String descri,
            @PathParam("numero") String numero,
            @PathParam("nombre") String nombre,
            @PathParam("cedula") String cedula) {
        Response<String> response = new Response<>();
        try {
            cie.resetAll();
            cie.initialize();
            cie.feedBack((byte) 2);

            cie.newLine();
            cie.setSize((byte) 25);
            cie.setFont((byte) 23);
            cie.alignCenter();
            cie.setText(descri);

            cie.newLine();
            cie.newLine();
//            cie.newLine();
            cie.alignCenter();
            cie.setSize((byte) 60);
            cie.setFont((byte) 23);
            cie.setText(numero);

            cie.newLine();
            cie.newLine();
            cie.newLine();
            cie.newLine();
            cie.setSize((byte) 15);
            cie.setFont((byte) 31);
            cie.alignCenter();
            cie.setText(cedula);

            cie.newLine();
            cie.setSize((byte) 15);
            cie.setFont((byte) 23);
            cie.alignCenter();
            nombre = cie.encodingAlambrado(nombre.toUpperCase());
            cie.setText(nombre);

            cie.newLine();
            cie.newLine();
            cie.newLine();
            cie.alignCenter();
            cie.setSize((byte) 15);
            cie.setFont((byte) 23);
            cie.setText("La Mutual de Funcionarios del MSP Y BS");

            cie.feed((byte) 2);
            cie.finit();

            System.out.println("TICKET 02 " + cie.getCommandSet());
            Ticket tic = new Ticket();
            tic.bytes = cie.getCommandSet().getBytes();
            tic.print();
            response.setCodigo(200);
            response.setData("OK");
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        } finally {
        }
    }

//    public void setNegro() {
//        try {
//            char[] ESC_CUT_PAPER = new char[]{0x1B, 'r', 0};
//            if (!this.dispositivo.trim().equals("pantalla.txt")) {
//                pw.write(ESC_CUT_PAPER);
//            }
//
//        } catch (Exception e) {
//            System.out.print(e);
//        }
//    }
//    
//    public void setFormato(int formato) {
//        try {
//            char[] ESC_CUT_PAPER = new char[]{0x1B, '!', (char) formato};
//            if (!this.dispositivo.trim().equals("pantalla.txt")) {
//                pw.write(ESC_CUT_PAPER);
//            }
//
//        } catch (Exception e) {
//            System.out.print(e);
//        }
//    }
//    
//    public void cortar() {
//        try {
//
//            char[] ESC_CUT_PAPER = new char[]{0x1B, 'm'};
//            if (!this.dispositivo.trim().equals("pantalla.txt")) {
//                pw.write(ESC_CUT_PAPER);
//            }
//
//        } catch (Exception e) {
//            System.out.print(e);
//        }
//
//    }
//    
//    public void salto() {
//        try {
//
//            pw.println("");
//
//        } catch (Exception e) {
//            System.out.print(e);
//
//        }
//
//    }
    @GET
    @Path("listarListo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RetiroPedidoDTO>> listarListo() {
        Response<List<RetiroPedidoDTO>> response = new Response<>();
        String sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=TRUE ORDER BY id_retiro ASC;";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                RetiroPedidoDTO socio = new RetiroPedidoDTO();
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    SociosDto soc = new SociosDto();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    PersonaDto soc = new PersonaDto();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
                listHere.add(socio);
            }
            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("listarAnoPeriodo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<AnoPeriodo>> listarAnoPeriodo() {
        Response<List<AnoPeriodo>> response = new Response<>();
        String sql = "Select * from turnero.anoperiodo WHERE estado=TRUE ORDER BY id DESC;";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<AnoPeriodo> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                AnoPeriodo socio = new AnoPeriodo();
                socio.setId(Long.parseLong(objects[0].toString()));
                socio.setDescripcion(objects[1].toString());
                socio.setEstado(Boolean.parseBoolean(objects[2].toString()));

                listHere.add(socio);
            }
            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @GET
    @Path("actualizarRecall/{id}/{box}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> actualizarRecall(@PathParam("id") Long id,
            @PathParam("box") String box) {
        Response<Boolean> response = new Response<>();
        RetiroPedido socio = new RetiroPedido();
        String sql = "Select * from turnero.retiro_pedido WHERE id_retiro=" + id + ";";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
//            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    Socio soc = new Socio();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    Persona soc = new Persona();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
//                listHere.add(socio);
            }
            boolean actualizar = false;

            if (socio.getCaja() == null) {
                actualizar = true;
            }

            socio.setEntregado(false);
            socio.setPreparacion(true);
            socio.setListo(false);
            socio.setLlamar(!socio.getLlamar());

            if (!actualizar && socio.getCaja().equalsIgnoreCase(box)) {
                actualizar = true;
            }
            socio.setCaja(box);

            if (actualizar) {
                em.merge(socio);
            }

            response.setCodigo(200);
            response.setData(true);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(false);
            return response;
        }
    }

    @GET
    @Transactional
    @Path("registrarPersona/{ci}/{nombre}/{apellido}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> registrarPersona(@PathParam("ci") String ci,
            @PathParam("nombre") String nombre,
            @PathParam("apellido") String apellido) {
        Response<Boolean> response = new Response<>();
        RetiroPedido socio = new RetiroPedido();
        String sql = "Select * from public.persona WHERE upper(ruc) LIKE '" + ci + "%';";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
//            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            if (list.size() <= 0) {
                Persona p = new Persona();
                p.setNombre(nombre);
                if (!apellido.equalsIgnoreCase("null")) {
                    p.setApellido(apellido);
                    p.setRazonsocial(nombre + " " + apellido);
                } else {
                    p.setRazonsocial(nombre);
                }
                p.setRuc(ci);
                p.setCedula(ci);
                p.setIdciudad(1l);
                p.setIdpersoneria(99l);
                p.setIdplancuenta(0l);
                p.setIdsocio(0l);
                long idR = recuperarId();
                p.setId(idR);
                em.persist(p);
            }

            response.setCodigo(200);
            response.setData(true);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(false);
            return response;
        }
    }

    @GET
    @Path("actualizarListo/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> actualizarListo(@PathParam("id") Long id) {
        Response<Boolean> response = new Response<>();
        RetiroPedido socio = new RetiroPedido();
        String sql = "Select * from turnero.retiro_pedido WHERE id_retiro=" + id + ";";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
//            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    Socio soc = new Socio();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    Persona soc = new Persona();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
//                listHere.add(socio);
            }
            socio.setEntregado(false);
            socio.setPreparacion(true);
            socio.setListo(true);
            socio.setLlamar(false);

            em.merge(socio);

            response.setCodigo(200);
            response.setData(true);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(false);
            return response;
        }
    }

    @GET
    @Path("actualizarEntregado/{id}/{idFunc}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<Boolean> actualizarEntregado(@PathParam("id") Long id, @PathParam("idFunc") Long idFunc) {
        Response<Boolean> response = new Response<>();
        RetiroPedido socio = new RetiroPedido();
        String sql = "Select * from turnero.retiro_pedido WHERE id_retiro=" + id + ";";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
//            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    Socio soc = new Socio();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    Persona soc = new Persona();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    Funcionario soc = new Funcionario();
                    soc.setId(idFunc);
                    socio.setFuncionario(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
//                listHere.add(socio);
            }
            socio.setEntregado(true);
            socio.setPreparacion(true);
            socio.setListo(true);
            socio.setLlamar(false);
            socio.setFecha(new Date());

            em.merge(socio);

            response.setCodigo(200);
            response.setData(true);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(false);
            return response;
        }
    }

    @GET
    @Path("listarFullListo/{serie}/{box}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RetiroPedidoDTO>> listarFullListo(@PathParam("serie") String serie,
            @PathParam("box") String box) {
        Response<List<RetiroPedidoDTO>> response = new Response<>();
//        RetiroPedidoDTO socio = new RetiroPedidoDTO();
        String sql = "";
        if (serie.equalsIgnoreCase("CR")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=TRUE AND "
                    + " (UPPER(serie) LIKE 'CR' OR "
                    + " UPPER(serie) LIKE 'AH' OR UPPER(serie) LIKE 'RE' OR "
                    + " UPPER(serie) LIKE 'OC' OR UPPER(serie) LIKE 'SO') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else if (serie.equalsIgnoreCase("TL")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=TRUE AND "
                    + " (UPPER(serie) LIKE 'TL' OR "
                    + " UPPER(serie) LIKE 'SR' OR UPPER(serie) LIKE 'CE') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else if (serie.equalsIgnoreCase("AD")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=TRUE AND "
                    + " (UPPER(serie) LIKE 'AD' OR "
                    + " UPPER(serie) LIKE 'AH') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=TRUE AND UPPER(serie) LIKE '" + serie.toUpperCase() + "' AND (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        }

        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                RetiroPedidoDTO socio = new RetiroPedidoDTO();
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    SociosDto soc = new SociosDto();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    PersonaDto soc = new PersonaDto();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
                listHere.add(socio);
            }

            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(new ArrayList<RetiroPedidoDTO>());
            return response;
        }
    }

    @GET
    @Path("listarFullPreparacionSerie/{serie}/{box}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RetiroPedidoDTO>> listarFullPreparacionSerie(@PathParam("serie") String serie,
            @PathParam("box") String box) {
        Response<List<RetiroPedidoDTO>> response = new Response<>();
//        RetiroPedidoDTO socio = new RetiroPedidoDTO();
        String sql = "";
        if (serie.equalsIgnoreCase("CR")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=FALSE AND "
                    + " (UPPER(serie) LIKE 'CR' OR "
                    + " UPPER(serie) LIKE 'AH' OR UPPER(serie) LIKE 'RE' OR "
                    + " UPPER(serie) LIKE 'OC' OR UPPER(serie) LIKE 'SO') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else if (serie.equalsIgnoreCase("TL")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=FALSE AND "
                    + " (UPPER(serie) LIKE 'TL' OR "
                    + " UPPER(serie) LIKE 'SR' OR UPPER(serie) LIKE 'CE') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else if (serie.equalsIgnoreCase("AD")) {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=FALSE AND "
                    + " (UPPER(serie) LIKE 'AD' OR "
                    + " UPPER(serie) LIKE 'AH') AND "
                    + " (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        } else {
            sql = "Select * from turnero.retiro_pedido WHERE entregado=FALSE AND preparacion=TRUE AND listo=FALSE AND UPPER(serie) LIKE '" + serie.toUpperCase() + "' AND (upper(caja) LIKE '" + box.toUpperCase() + "' OR caja isnull) ORDER BY id_retiro ASC";
        }
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                RetiroPedidoDTO socio = new RetiroPedidoDTO();
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    SociosDto soc = new SociosDto();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    PersonaDto soc = new PersonaDto();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
                listHere.add(socio);
            }

            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setData(new ArrayList<RetiroPedidoDTO>());
            return response;
        }
    }

    @GET
    @Path("listarLlamados")
    @Produces(MediaType.APPLICATION_JSON)
    public Response<List<RetiroPedidoDTO>> listarLlamados() {
        Response<List<RetiroPedidoDTO>> response = new Response<>();
//        RetiroPedidoDTO socio = new RetiroPedidoDTO();
        String sql = "Select * from turnero.retiro_pedido WHERE llamar=TRUE;";
        try {
            Query query = em.createNativeQuery(sql);
            List<Object[]> list = query.getResultList();
            List<RetiroPedidoDTO> listHere = new ArrayList<>();
            for (Object[] objects : list) {
                RetiroPedidoDTO socio = new RetiroPedidoDTO();
                socio.setNumero(objects[0].toString());
                socio.setCliente(objects[1].toString());
                socio.setEntregado(Boolean.parseBoolean(objects[2].toString()));
                socio.setPreparacion(Boolean.parseBoolean(objects[3].toString()));
                socio.setListo(Boolean.parseBoolean(objects[4].toString()));
                socio.setHora(objects[5].toString());
                socio.setSerie(objects[6].toString());
                try {
                    socio.setCaja(objects[7].toString());
                } catch (Exception e) {
                    socio.setCaja(null);
                } finally {
                }
                socio.setLlamar(Boolean.parseBoolean(objects[8].toString()));
                socio.setIdRetiro(Long.parseLong(objects[9].toString()));
                try {
                    SociosDto soc = new SociosDto();
                    soc.setId(Long.parseLong(objects[10].toString()));
                    socio.setSocio(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    PersonaDto soc = new PersonaDto();
                    soc.setId(Long.parseLong(objects[13].toString()));
                    socio.setPersona(soc);
                } catch (Exception e) {
                } finally {
                }
                try {
                    socio.setCi(objects[11].toString());
                } catch (Exception e) {
                    socio.setCi(null);
                } finally {
                }
                listHere.add(socio);
            }
            response.setCodigo(200);
            response.setData(listHere);
            response.setMensaje("Proceso satisfactorio");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(401);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    private Long recuperarId() {
        String sql = "Select MAX(id) id from public.persona;";
        long numero = 0l;
        try {
            Query query = em.createNativeQuery(sql);
            List<BigDecimal> list = query.getResultList();
            numero = list.get(0).longValue();
            return (numero + 1);
        } catch (Exception e) {
            return null;
        }
    }
}
