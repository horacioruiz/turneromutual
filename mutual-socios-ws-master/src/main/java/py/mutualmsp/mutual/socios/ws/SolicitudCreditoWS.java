/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.jboss.logging.Logger;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import py.mutualmsp.mutual.socios.ws.dto.Response;
import py.mutualmsp.mutual.socios.ws.entities.Socio;
import py.mutualmsp.mutual.socios.ws.entities.SolicitudCredito;
import py.mutualmsp.mutual.socios.ws.entities.SolicitudDocumentos;
import py.mutualmsp.mutual.socios.ws.entities.Tipocredito;
import py.mutualmsp.mutual.socios.ws.util.Constants;
import py.mutualmsp.mutual.socios.ws.util.DBConnection;
import py.mutualmsp.mutual.socios.ws.util.DBConnectionSimple;
import py.mutualmsp.mutual.socios.ws.util.Util;

/**
 *
 * @author hectorvillalba
 */
@Stateless
@Path("solicitud")
public class SolicitudCreditoWS implements Serializable {

    @PersistenceContext
    private EntityManager em;
    Logger log = Logger.getLogger(ConsultaSociosWS.class.getSimpleName());

    public class MyRunnable implements Runnable {

        Long idTipo;
        SolicitudCredito credito;

        public MyRunnable(SolicitudCredito sc, Long idTipo) {
            // store parameter for later user
            this.idTipo = idTipo;
            this.credito = sc;
        }

        public void run() {
            try {
                Tipocredito tipocredito = (Tipocredito) em.createQuery("select m from Tipocredito m where m.id = :id")
                        .setParameter("id", idTipo)
                        .getSingleResult();

                DBConnectionSimple dBConnection = new DBConnectionSimple();
                try {
                    Statement statement = dBConnection.getConnection().createStatement();
                    String sql = "UPDATE solicitud_credito SET idtipoCredito=" + tipocredito.getId() + " WHERE id=" + credito.getId();
                    System.out.println(sql);
                    int i = statement.executeUpdate(sql);
                } catch (SQLException ex) {
                    System.err.println(ex.getMessage());
                }
            } catch (Exception e) {
                System.out.println("ERROR: " + e.getLocalizedMessage());
                System.out.println("ERROR: " + e.fillInStackTrace());
            } finally {
            }
        }
    }

    @POST
    @Path("save/{idTipo}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response<SolicitudCredito> save(@PathParam("idTipo") Long idTipo, SolicitudCredito credito) {
        Response<SolicitudCredito> response = new Response<>();
        try {
            Socio marcacion = null;
            try {
                marcacion = (Socio) em.createQuery("select m from Socio m where m.cedula = :id")
                        .setParameter("id", credito.getNrodocumento())
                        .getSingleResult();
            } catch (Exception e) {
            } finally {
            }

            if (marcacion != null) {
                log.info("### SAVE ###");
                log.info("Nombre: " + credito.getNombre());
                log.info("Apellido: " + credito.getApellido());
                credito.setIdSocio(Integer.parseInt(marcacion.getId() + ""));
                credito.setNombre(marcacion.getNombre());
                credito.setApellido(marcacion.getApellido());
                credito.setFecha(new Date());
                em.persist(credito);
                em.flush();
                response.setCodigo(200);
                response.setData(credito);
                response.setMensaje("Los datos ya han sido procesados correctamente. Muchas gracias !");

                Runnable r = new MyRunnable(credito, idTipo);
                new Thread(r).start();

                return response;
            } else {
                response.setCodigo(400);
                response.setMensaje("Nro de socio ingresado no existe");
                return response;
            }

        } catch (Exception e) {
            response.setCodigo(400);
            response.setMensaje("Error: No se insertaron los datos, intentelo mas tarde");
            return response;
        }
    }

    @POST
    @Path("uploadImage/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response<SolicitudCredito> upLoadImage(@PathParam("id") Long id, MultipartFormDataInput input) {
        Response<SolicitudCredito> response = new Response<>();
        try {
            log.info("### upLoadImage ###");
            log.info("ID: " + id);
            SolicitudCredito marcacion = (SolicitudCredito) em.createQuery("select m from SolicitudCredito m where m.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            // Guardamos el archivo en disco
            Map<String, String> map = Util.saveFileToDisk(
                    input,
                    Constants.UPLOAD_DIR + "/mutualmspybs/",
                    id.toString()
            );
            SolicitudDocumentos solicitudDocumentos = new SolicitudDocumentos();
            solicitudDocumentos.setUrlCiFrontal(Constants.PUBLIC_SERVER_URL + "/mutualmspybs/" + map.get("fileName").toString());
            solicitudDocumentos.setSolicitudCredito(marcacion);
            em.persist(solicitudDocumentos);
            //archivoDet.setUrl(Constants.PUBLIC_SERVER_URL + tipoArchivoEnum.getUploadDir() + fileName);
            response.setCodigo(200);
            response.setData(marcacion);
            response.setMensaje("Los datos ya han sido procesados correctamente. Muchas gracias !");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }

    @POST
    @Path("uploadImage/{num}/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("multipart/form-data")
    public Response<SolicitudCredito> upLoadImageByNum(@PathParam("num") Long num, @PathParam("id") Long id, MultipartFormDataInput input) {
        Response<SolicitudCredito> response = new Response<>();
        try {
            log.info("### upLoadImage ###");
            log.info("ID: " + id);

            SolicitudCredito marcacion = (SolicitudCredito) em.createQuery("select m from SolicitudCredito m where m.id = :id")
                    .setParameter("id", id)
                    .getSingleResult();
            // Guardamos el archivo en disco
            Map<String, String> map = Util.saveFileToDiskImage(
                    input,
                    Constants.UPLOAD_DIR + "/mutualmspybs/",
                    id.toString(),
                    Integer.parseInt(num + "")
            );
            SolicitudDocumentos solicitudDocumentos = null;
            try {
                solicitudDocumentos = (SolicitudDocumentos) em.createQuery("select m from SolicitudDocumentos m JOIN FETCH m.solicitudCredito sc where sc.id = :id")
                        .setParameter("id", id)
                        .getSingleResult();
            } catch (Exception e) {
            } finally {
            }

            if (solicitudDocumentos == null) {
                solicitudDocumentos = new SolicitudDocumentos();
            }

            if (num == 1) {
                solicitudDocumentos.setUrlCiFrontal(Constants.UPLOAD_DIR + "\\mutualmspybs\\\\" + map.get("fileName").toString());
            } else if (num == 2) {
                solicitudDocumentos.setUrlCiDorso(Constants.UPLOAD_DIR + "\\mutualmspybs\\" + map.get("fileName").toString());
            } else if (num == 3) {
                solicitudDocumentos.setUrlLiquidacion(Constants.UPLOAD_DIR + "\\mutualmspybs\\" + map.get("fileName").toString());
            }

            solicitudDocumentos.setSolicitudCredito(marcacion);
            em.merge(solicitudDocumentos);
            //archivoDet.setUrl(Constants.PUBLIC_SERVER_URL + tipoArchivoEnum.getUploadDir() + fileName);
            response.setCodigo(200);
            response.setData(marcacion);
            response.setMensaje("Los datos ya han sido procesados correctamente. Muchas gracias !");
            return response;
        } catch (Exception e) {
            e.printStackTrace();
            response.setCodigo(400);
            response.setMensaje("Error: " + e.getMessage());
            return response;
        }
    }
}
