/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "detalleoperacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalleoperacion.findAll", query = "SELECT d FROM Detalleoperacion d"),
    @NamedQuery(name = "Detalleoperacion.findById", query = "SELECT d FROM Detalleoperacion d WHERE d.id = :id"),
    @NamedQuery(name = "Detalleoperacion.findByNumerocuota", query = "SELECT d FROM Detalleoperacion d WHERE d.numerocuota = :numerocuota"),
    @NamedQuery(name = "Detalleoperacion.findByFechavencimiento", query = "SELECT d FROM Detalleoperacion d WHERE d.fechavencimiento = :fechavencimiento"),
    @NamedQuery(name = "Detalleoperacion.findByMontocapital", query = "SELECT d FROM Detalleoperacion d WHERE d.montocapital = :montocapital"),
    @NamedQuery(name = "Detalleoperacion.findByMontointeres", query = "SELECT d FROM Detalleoperacion d WHERE d.montointeres = :montointeres"),
    @NamedQuery(name = "Detalleoperacion.findBySaldocapital", query = "SELECT d FROM Detalleoperacion d WHERE d.saldocapital = :saldocapital"),
    @NamedQuery(name = "Detalleoperacion.findBySaldointeres", query = "SELECT d FROM Detalleoperacion d WHERE d.saldointeres = :saldointeres"),
    @NamedQuery(name = "Detalleoperacion.findByGiraduria", query = "SELECT d FROM Detalleoperacion d WHERE d.giraduria = :giraduria"),
    @NamedQuery(name = "Detalleoperacion.findByInteres", query = "SELECT d FROM Detalleoperacion d WHERE d.interes = :interes"),
    @NamedQuery(name = "Detalleoperacion.findByCantidaddia", query = "SELECT d FROM Detalleoperacion d WHERE d.cantidaddia = :cantidaddia"),
    @NamedQuery(name = "Detalleoperacion.findByIdmovimiento", query = "SELECT d FROM Detalleoperacion d WHERE d.idmovimiento = :idmovimiento"),
    @NamedQuery(name = "Detalleoperacion.findByTabla", query = "SELECT d FROM Detalleoperacion d WHERE d.tabla = :tabla")})
public class Detalleoperacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerocuota")
    private long numerocuota;
    @Column(name = "fechavencimiento")
    @Temporal(TemporalType.DATE)
    private Date fechavencimiento;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "montocapital")
    private BigDecimal montocapital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "montointeres")
    private BigDecimal montointeres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldocapital")
    private BigDecimal saldocapital;
    @Basic(optional = false)
    @NotNull
    @Column(name = "saldointeres")
    private BigDecimal saldointeres;
    @Size(max = 1)
    @Column(name = "giraduria")
    private String giraduria;
    @Basic(optional = false)
    @NotNull
    @Column(name = "interes")
    private BigDecimal interes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidaddia")
    private long cantidaddia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idmovimiento")
    private long idmovimiento;
    @Size(max = 2)
    @Column(name = "tabla")
    private String tabla;

    public Detalleoperacion() {
    }

    public Detalleoperacion(Long id) {
        this.id = id;
    }

    public Detalleoperacion(Long id, long numerocuota, BigDecimal montocapital, BigDecimal montointeres, BigDecimal saldocapital, BigDecimal saldointeres, BigDecimal interes, long cantidaddia, long idmovimiento) {
        this.id = id;
        this.numerocuota = numerocuota;
        this.montocapital = montocapital;
        this.montointeres = montointeres;
        this.saldocapital = saldocapital;
        this.saldointeres = saldointeres;
        this.interes = interes;
        this.cantidaddia = cantidaddia;
        this.idmovimiento = idmovimiento;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getNumerocuota() {
        return numerocuota;
    }

    public void setNumerocuota(long numerocuota) {
        this.numerocuota = numerocuota;
    }

    public Date getFechavencimiento() {
        return fechavencimiento;
    }

    public void setFechavencimiento(Date fechavencimiento) {
        this.fechavencimiento = fechavencimiento;
    }

    public BigDecimal getMontocapital() {
        return montocapital;
    }

    public void setMontocapital(BigDecimal montocapital) {
        this.montocapital = montocapital;
    }

    public BigDecimal getMontointeres() {
        return montointeres;
    }

    public void setMontointeres(BigDecimal montointeres) {
        this.montointeres = montointeres;
    }

    public BigDecimal getSaldocapital() {
        return saldocapital;
    }

    public void setSaldocapital(BigDecimal saldocapital) {
        this.saldocapital = saldocapital;
    }

    public BigDecimal getSaldointeres() {
        return saldointeres;
    }

    public void setSaldointeres(BigDecimal saldointeres) {
        this.saldointeres = saldointeres;
    }

    public String getGiraduria() {
        return giraduria;
    }

    public void setGiraduria(String giraduria) {
        this.giraduria = giraduria;
    }

    public BigDecimal getInteres() {
        return interes;
    }

    public void setInteres(BigDecimal interes) {
        this.interes = interes;
    }

    public long getCantidaddia() {
        return cantidaddia;
    }

    public void setCantidaddia(long cantidaddia) {
        this.cantidaddia = cantidaddia;
    }

    public long getIdmovimiento() {
        return idmovimiento;
    }

    public void setIdmovimiento(long idmovimiento) {
        this.idmovimiento = idmovimiento;
    }

    public String getTabla() {
        return tabla;
    }

    public void setTabla(String tabla) {
        this.tabla = tabla;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Detalleoperacion)) {
            return false;
        }
        Detalleoperacion other = (Detalleoperacion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Detalleoperacion[ id=" + id + " ]";
    }
    
}
