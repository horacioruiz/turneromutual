/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "solicitud_credito")
@XmlRootElement
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SolicitudCredito implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "idsocio")
    private Integer idSocio;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "telefono")
    private String telefono;

    @Column(name = "nrodocumento")
    private String nrodocumento;

    @Column(name = "donde_se_entero")
    private String dondeSeEntero;
    
    @Column(name = "nropin")
    private String nropin;

    @Column(name = "fecha")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date fecha;

    @Column(name = "estado")
    private String estado;

    @Column(name = "retiro_credito")
    private String retiroCredito;

    @Column(name = "monto")
    private Double monto;

    @JoinColumn(name = "idtipoCredito", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Tipocredito tipoCredito;

    @JoinColumn(name = "idcargo", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Cargo cargo;

    @JoinColumn(name = "idrepresentante", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Representante representante;

    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institucion institucion;

    @JoinColumn(name = "idregional", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Regional regional;

    public String getDondeSeEntero() {
        return dondeSeEntero;
    }

    public void setDondeSeEntero(String dondeSeEntero) {
        this.dondeSeEntero = dondeSeEntero;
    }

    @Column(name = "rubro")
    private String rubro;

    @Column(name = "plazo")
    private Integer plazo;

    @JoinColumn(name = "iddestino", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Destino destino;
    
    @Column(name = "idmovimiento")
    private Long idmovimiento;
    
    @Column(name = "origen")
    private String origen;
    
    @Column(name = "retiro")
    private String retiro;

    public String getRetiro() {
        return retiro;
    }

    public void setRetiro(String retiro) {
        this.retiro = retiro;
    }

    
    public Long getIdmovimiento() {
        return idmovimiento;
    }

    public void setIdmovimiento(Long idmovimiento) {
        this.idmovimiento = idmovimiento;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }
    
    

    public Integer getPlazo() {
        return plazo;
    }

    public void setPlazo(Integer plazo) {
        this.plazo = plazo;
    }

    public Destino getDestino() {
        return destino;
    }

    public void setDestino(Destino destino) {
        this.destino = destino;
    }

    public String getRubro() {
        return rubro;
    }

    public String getRetiroCredito() {
        return retiroCredito;
    }

    public void setRetiroCredito(String retiroCredito) {
        this.retiroCredito = retiroCredito;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Representante getRepresentante() {
        return representante;
    }

    public void setRepresentante(Representante representante) {
        this.representante = representante;
    }

    public Institucion getInstitucion() {
        return institucion;
    }

    public void setInstitucion(Institucion institucion) {
        this.institucion = institucion;
    }

    public Regional getRegional() {
        return regional;
    }

    public void setRegional(Regional regional) {
        this.regional = regional;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Integer idSocio) {
        this.idSocio = idSocio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getNrodocumento() {
        return nrodocumento;
    }

    public void setNrodocumento(String nrodocumento) {
        this.nrodocumento = nrodocumento;
    }

    public String getNropin() {
        return nropin;
    }

    public void setNropin(String nropin) {
        this.nropin = nropin;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Double getMonto() {
        return monto;
    }

    public void setMonto(Double monto) {
        this.monto = monto;
    }

    public Tipocredito getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(Tipocredito tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

}
