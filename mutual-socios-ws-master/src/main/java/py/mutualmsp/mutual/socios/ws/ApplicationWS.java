/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws;

import java.util.HashSet;
import java.util.Set;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import org.jboss.resteasy.plugins.interceptors.CorsFilter;

/**
 *
 * @author hectorvillalba
 */
@ApplicationPath("api")
public class ApplicationWS extends Application {

    private Set<Object> singletons = new HashSet<Object>();
    private HashSet<Class<?>> classes = new HashSet<Class<?>>();

    public ApplicationWS() {
        CorsFilter corsFilter = new CorsFilter();
        corsFilter.getAllowedOrigins().add("*");
        corsFilter.setAllowedMethods("OPTIONS, GET, POST, DELETE, PUT, PATCH");
        corsFilter.setAllowedHeaders("Content-Type, Authorization");
        singletons.add(corsFilter);
        classes.add(SolicitudCreditoWS.class);
        classes.add(ConsultaDefiniciones.class);
        classes.add(ConsultaSociosWS.class);
        classes.add(FuncionariosWS.class);
        classes.add(RegistroWS.class);
        classes.add(SolicitudAyudaWS.class);
        classes.add(PublicidadWS.class);
        classes.add(RangoRetiroPedidoWS.class);
        classes.add(RetiroPedidoWS.class);
        //classes.add(UsersResource.class);
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

    @Override
    public HashSet<Class<?>> getClasses() {
        return classes;
    }

}
