/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "solicitud_ayuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SolicitudAyuda.findAll", query = "SELECT s FROM SolicitudAyuda s")
    , @NamedQuery(name = "SolicitudAyuda.findById", query = "SELECT s FROM SolicitudAyuda s WHERE s.id = :id")
    , @NamedQuery(name = "SolicitudAyuda.findByFecha", query = "SELECT s FROM SolicitudAyuda s WHERE s.fecha = :fecha")
    , @NamedQuery(name = "SolicitudAyuda.findByTelefono", query = "SELECT s FROM SolicitudAyuda s WHERE s.telefono = :telefono")
    , @NamedQuery(name = "SolicitudAyuda.findByUrlFoto1", query = "SELECT s FROM SolicitudAyuda s WHERE s.urlFoto1 = :urlFoto1")
    , @NamedQuery(name = "SolicitudAyuda.findByUrlFoto2", query = "SELECT s FROM SolicitudAyuda s WHERE s.urlFoto2 = :urlFoto2")
    , @NamedQuery(name = "SolicitudAyuda.findByUrlFoto3", query = "SELECT s FROM SolicitudAyuda s WHERE s.urlFoto3 = :urlFoto3")
    , @NamedQuery(name = "SolicitudAyuda.findByEstado", query = "SELECT s FROM SolicitudAyuda s WHERE s.estado = :estado")
    , @NamedQuery(name = "SolicitudAyuda.findByObservacion", query = "SELECT s FROM SolicitudAyuda s WHERE s.observacion = :observacion")
    , @NamedQuery(name = "SolicitudAyuda.findByAtendidopor", query = "SELECT s FROM SolicitudAyuda s WHERE s.atendidopor = :atendidopor")
    , @NamedQuery(name = "SolicitudAyuda.findByTelefonoatencion", query = "SELECT s FROM SolicitudAyuda s WHERE s.telefonoatencion = :telefonoatencion")
    , @NamedQuery(name = "SolicitudAyuda.findByMotivo", query = "SELECT s FROM SolicitudAyuda s WHERE s.motivo = :motivo")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SolicitudAyuda implements Serializable {

    @Size(max = 200)
    @Column(name = "cargo")
    private String cargo;
    @JoinColumn(name = "idinstitucion", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Institucion idinstitucion;

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha")
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private Date fecha;
    @Size(max = 20)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 2147483647)
    @Column(name = "url_foto_1")
    private String urlFoto1;
    @Size(max = 2147483647)
    @Column(name = "url_foto_2")
    private String urlFoto2;
    @Size(max = 2147483647)
    @Column(name = "url_foto_3")
    private String urlFoto3;
    @Size(max = 20)
    @Column(name = "estado")
    private String estado;
    @Size(max = 800)
    @Column(name = "observacion")
    private String observacion;
    @Size(max = 200)
    @Column(name = "atendidopor")
    private String atendidopor;
    @Size(max = 200)
    @Column(name = "telefonoatencion")
    private String telefonoatencion;
    @Size(max = 200)
    @Column(name = "motivo_obs")
    private String motivo;
    @JoinColumn(name = "idciudad", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Ciudad idciudad;
    @JoinColumn(name = "iddepartamento", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Departamento iddepartamento;
    @JoinColumn(name = "id_motivo_solicitud", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private MotivoSolicitudAyuda idMotivoSolicitud;
    @JoinColumn(name = "idsocio", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Socio idsocio;
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Usuario idUsuario;

    @JoinColumn(name = "id_necesidad", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private NecesidadAyuda idNecesidad;

    @Transient
    private List<DatosFamiliares> datosFamiliares;

    @Transient
    private List<DatosSocioDemograficos> datosSociosDemograficos;

    public List<DatosSocioDemograficos> getDatosSociosDemograficos() {
        return datosSociosDemograficos;
    }

    public void setDatosSociosDemograficos(List<DatosSocioDemograficos> datosSociosDemograficos) {
        this.datosSociosDemograficos = datosSociosDemograficos;
    }

    public List<DatosFamiliares> getDatosFamiliares() {
        return datosFamiliares;
    }

    public void setDatosFamiliares(List<DatosFamiliares> datosFamiliares) {
        this.datosFamiliares = datosFamiliares;
    }

    @Column(name = "persona_dep_mayor")
    private Integer personaDepMayor;

    @Column(name = "persona_dep_menor")
    private Integer personaDepMenor;

    @Size(max = 800)
    @Column(name = "necesidad_obs")
    private String necesidadObs;

    @Size(max = 250)
    @Column(name = "tipo_vivienda")
    private String tipoVivienda;

    @Size(max = 800)
    @Column(name = "url_liquidacion")
    private String urlLiquidacion;

    @Size(max = 800)
    @Column(name = "pin_liquidacion")
    private String pinLiquidacion;

    public String getUrlLiquidacion() {
        return urlLiquidacion;
    }

    public void setUrlLiquidacion(String urlLiquidacion) {
        this.urlLiquidacion = urlLiquidacion;
    }

    public String getPinLiquidacion() {
        return pinLiquidacion;
    }

    public void setPinLiquidacion(String pinLiquidacion) {
        this.pinLiquidacion = pinLiquidacion;
    }

    public Integer getPersonaDepMayor() {
        return personaDepMayor;
    }

    public void setPersonaDepMayor(Integer personaDepMayor) {
        this.personaDepMayor = personaDepMayor;
    }

    public Integer getPersonaDepMenor() {
        return personaDepMenor;
    }

    public void setPersonaDepMenor(Integer personaDepMenor) {
        this.personaDepMenor = personaDepMenor;
    }

    public String getNecesidadObs() {
        return necesidadObs;
    }

    public void setNecesidadObs(String necesidadObs) {
        this.necesidadObs = necesidadObs;
    }

    public String getTipoVivienda() {
        return tipoVivienda;
    }

    public void setTipoVivienda(String tipoVivienda) {
        this.tipoVivienda = tipoVivienda;
    }

    public NecesidadAyuda getIdNecesidad() {
        return idNecesidad;
    }

    public void setIdNecesidad(NecesidadAyuda idNecesidad) {
        this.idNecesidad = idNecesidad;
    }

    public SolicitudAyuda() {
    }

    public SolicitudAyuda(Long id) {
        this.id = id;
    }

    public SolicitudAyuda(Long id, Date fecha) {
        this.id = id;
        this.fecha = fecha;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUrlFoto1() {
        return urlFoto1;
    }

    public void setUrlFoto1(String urlFoto1) {
        this.urlFoto1 = urlFoto1;
    }

    public String getUrlFoto2() {
        return urlFoto2;
    }

    public void setUrlFoto2(String urlFoto2) {
        this.urlFoto2 = urlFoto2;
    }

    public String getUrlFoto3() {
        return urlFoto3;
    }

    public void setUrlFoto3(String urlFoto3) {
        this.urlFoto3 = urlFoto3;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getAtendidopor() {
        return atendidopor;
    }

    public void setAtendidopor(String atendidopor) {
        this.atendidopor = atendidopor;
    }

    public String getTelefonoatencion() {
        return telefonoatencion;
    }

    public void setTelefonoatencion(String telefonoatencion) {
        this.telefonoatencion = telefonoatencion;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public Ciudad getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(Ciudad idciudad) {
        this.idciudad = idciudad;
    }

    public Departamento getIddepartamento() {
        return iddepartamento;
    }

    public void setIddepartamento(Departamento iddepartamento) {
        this.iddepartamento = iddepartamento;
    }

    public MotivoSolicitudAyuda getIdMotivoSolicitud() {
        return idMotivoSolicitud;
    }

    public void setIdMotivoSolicitud(MotivoSolicitudAyuda idMotivoSolicitud) {
        this.idMotivoSolicitud = idMotivoSolicitud;
    }

    public Socio getIdsocio() {
        return idsocio;
    }

    public void setIdsocio(Socio idsocio) {
        this.idsocio = idsocio;
    }

    public Usuario getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Usuario idUsuario) {
        this.idUsuario = idUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudAyuda)) {
            return false;
        }
        SolicitudAyuda other = (SolicitudAyuda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.SolicitudAyuda[ id=" + id + " ]";
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public Institucion getIdinstitucion() {
        return idinstitucion;
    }

    public void setIdinstitucion(Institucion idinstitucion) {
        this.idinstitucion = idinstitucion;
    }

}
