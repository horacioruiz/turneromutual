/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.util;

/**
 *
 * @author Hruiz
 */
public class ComandosImpresoraEpson {

    public String commandSet = "";

    public ComandosImpresoraEpson() {
        this.commandSet = "";
    }

    public String initialize() {
        final byte[] Init = {27, 64};
        commandSet += new String(Init);
        return new String(Init);
    }

    public String feedBack(byte lines) {
        final byte[] Feed = {27, 101, lines};
        String s = new String(Feed);
        commandSet += s;
        return s;
    }

    public String newLine() {
        final byte[] LF = {10};
        String s = new String(LF);
        commandSet += s;
        return s;
    }

    public void setText(String s) {
        commandSet += s;
    }

    public String setSize(byte num) {
        final byte[] LF = {0x1B, 0x21, num};
        String s = new String(LF);
        commandSet += s;
        return s;
    }

    public String setFont(byte num) {
        final byte[] LF = {0x1B, 0x4D, num};
        String s = new String(LF);
        commandSet += s;
        return s;
    }

    public String feed(byte lines) {
        final byte[] Feed = {27, 100, lines};
        String s = new String(Feed);
        commandSet += s;
        return s;
    }

    public String finit() {
        final byte[] FeedAndCut = {29, 'V', 66, 0};

        String s = new String(FeedAndCut);

        final byte[] DrawerKick = {27, 70, 0, 60, 120};
        s += new String(DrawerKick);

        commandSet += s;
        return s;
    }

    public static String encodingAlambrado(String str) {
        str = str.replace('Á', 'A');
        str = str.replace('É', 'E');
        str = str.replace('Í', 'I');
        str = str.replace('Ó', 'O');
        str = str.replace('Ú', 'U');
        str = str.replace("Ñ", "N");
        return str;
    }

    public String alignLeft() {
        final byte[] AlignLeft = {27, 97, 48};
        String s = new String(AlignLeft);
        commandSet += s;
        return s;
    }

    public String alignCenter() {
        final byte[] AlignCenter = {27, 97, 49};
        String s = new String(AlignCenter);
        commandSet += s;
        return s;
    }

    public String alignRight() {
        final byte[] AlignRight = {27, 97, 50};
        String s = new String(AlignRight);
        commandSet += s;
        return s;
    }

    public void resetAll() {
        commandSet = "";
    }

    public String getCommandSet() {
        return commandSet;
    }

    public void setCommandSet(String commandSet) {
        this.commandSet = commandSet;
    }

}
