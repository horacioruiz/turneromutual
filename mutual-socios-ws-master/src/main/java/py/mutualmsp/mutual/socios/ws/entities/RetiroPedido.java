/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "retiro_pedido", schema = "turnero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RetiroPedido.findAll", query = "SELECT u FROM RetiroPedido u")
})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RetiroPedido implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_retiro")
    private Long idRetiro;

    @Column(name = "numero")
    private String numero;

    @Column(name = "ci")
    private String ci;

    @Column(name = "serie")
    private String serie;

    @Column(name = "caja")
    private String caja;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "entregado")
    private Boolean entregado;

    @Column(name = "preparacion")
    private Boolean preparacion;

    @Column(name = "listo")
    private Boolean listo;

    @Column(name = "hora")
    private String hora;

    @Column(name = "obs")
    private String obs;

    @Column(name = "llamar")
    private Boolean llamar;

    @Column(name = "fecha")
    private Date fecha;

    @JoinColumn(name = "idsocio", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Socio socio;

    @JoinColumn(name = "idpersona", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Persona persona;

    @JoinColumn(name = "idfuncionario", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Funcionario funcionario;

    public RetiroPedido() {
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRetiro != null ? idRetiro.hashCode() : 0);
        return hash;
    }

    public Long getIdRetiro() {
        return idRetiro;
    }

    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    public void setIdRetiro(Long idRetiro) {
        this.idRetiro = idRetiro;
    }

    public String getNumero() {
        return numero;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public Date getFecha() {
        return fecha;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Boolean getEntregado() {
        return entregado;
    }

    public void setEntregado(Boolean entregado) {
        this.entregado = entregado;
    }

    public Boolean getPreparacion() {
        return preparacion;
    }

    public void setPreparacion(Boolean preparacion) {
        this.preparacion = preparacion;
    }

    public Boolean getListo() {
        return listo;
    }

    public void setListo(Boolean listo) {
        this.listo = listo;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public Boolean getLlamar() {
        return llamar;
    }

    public void setLlamar(Boolean llamar) {
        this.llamar = llamar;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RetiroPedido)) {
            return false;
        }
        RetiroPedido other = (RetiroPedido) object;
        if ((this.idRetiro == null && other.idRetiro != null) || (this.idRetiro != null && !this.idRetiro.equals(other.idRetiro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.RetiroPedido[ id=" + idRetiro + " ]";
    }

}
