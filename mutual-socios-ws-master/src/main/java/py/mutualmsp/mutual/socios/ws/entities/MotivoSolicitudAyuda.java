/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "motivo_solicitud_ayuda")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MotivoSolicitudAyuda.findAll", query = "SELECT m FROM MotivoSolicitudAyuda m")
    , @NamedQuery(name = "MotivoSolicitudAyuda.findById", query = "SELECT m FROM MotivoSolicitudAyuda m WHERE m.id = :id")
    , @NamedQuery(name = "MotivoSolicitudAyuda.findByDescripcion", query = "SELECT m FROM MotivoSolicitudAyuda m WHERE m.descripcion = :descripcion")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class MotivoSolicitudAyuda implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Size(max = 800)
    @Column(name = "descripcion")
    private String descripcion;

    public MotivoSolicitudAyuda() {
    }

    public MotivoSolicitudAyuda(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MotivoSolicitudAyuda)) {
            return false;
        }
        MotivoSolicitudAyuda other = (MotivoSolicitudAyuda) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.MotivoSolicitudAyuda[ id=" + id + " ]";
    }
    
}
