/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "funcionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
    , @NamedQuery(name = "Funcionario.findById", query = "SELECT f FROM Funcionario f WHERE f.id = :id")
    , @NamedQuery(name = "Funcionario.findByNombre", query = "SELECT f FROM Funcionario f WHERE f.nombre = :nombre")
    , @NamedQuery(name = "Funcionario.findByApellido", query = "SELECT f FROM Funcionario f WHERE f.apellido = :apellido")
    , @NamedQuery(name = "Funcionario.findByIdtipofuncionario", query = "SELECT f FROM Funcionario f WHERE f.idtipofuncionario = :idtipofuncionario")
    , @NamedQuery(name = "Funcionario.findByIdestado", query = "SELECT f FROM Funcionario f WHERE f.idestado = :idestado")
    , @NamedQuery(name = "Funcionario.findByIdcargo", query = "SELECT f FROM Funcionario f WHERE f.idcargo = :idcargo")
    , @NamedQuery(name = "Funcionario.findByIdfilial", query = "SELECT f FROM Funcionario f WHERE f.idfilial = :idfilial")
    , @NamedQuery(name = "Funcionario.findByIddependencia", query = "SELECT f FROM Funcionario f WHERE f.iddependencia = :iddependencia")
    , @NamedQuery(name = "Funcionario.findByFechanacimiento", query = "SELECT f FROM Funcionario f WHERE f.fechanacimiento = :fechanacimiento")
    , @NamedQuery(name = "Funcionario.findByDireccion", query = "SELECT f FROM Funcionario f WHERE f.direccion = :direccion")
    , @NamedQuery(name = "Funcionario.findByTelefono", query = "SELECT f FROM Funcionario f WHERE f.telefono = :telefono")
    , @NamedQuery(name = "Funcionario.findByCelular", query = "SELECT f FROM Funcionario f WHERE f.celular = :celular")
    , @NamedQuery(name = "Funcionario.findByEmail", query = "SELECT f FROM Funcionario f WHERE f.email = :email")
    , @NamedQuery(name = "Funcionario.findByPatologia", query = "SELECT f FROM Funcionario f WHERE f.patologia = :patologia")
    , @NamedQuery(name = "Funcionario.findBySalario", query = "SELECT f FROM Funcionario f WHERE f.salario = :salario")
    , @NamedQuery(name = "Funcionario.findByFechaingreso", query = "SELECT f FROM Funcionario f WHERE f.fechaingreso = :fechaingreso")
    , @NamedQuery(name = "Funcionario.findByFechaips", query = "SELECT f FROM Funcionario f WHERE f.fechaips = :fechaips")
    , @NamedQuery(name = "Funcionario.findByFechasalida", query = "SELECT f FROM Funcionario f WHERE f.fechasalida = :fechasalida")
    , @NamedQuery(name = "Funcionario.findByIdpais", query = "SELECT f FROM Funcionario f WHERE f.idpais = :idpais")
    , @NamedQuery(name = "Funcionario.findByIdestadocivil", query = "SELECT f FROM Funcionario f WHERE f.idestadocivil = :idestadocivil")
    , @NamedQuery(name = "Funcionario.findByIdciudad", query = "SELECT f FROM Funcionario f WHERE f.idciudad = :idciudad")
    , @NamedQuery(name = "Funcionario.findByIdprofesion", query = "SELECT f FROM Funcionario f WHERE f.idprofesion = :idprofesion")
    , @NamedQuery(name = "Funcionario.findByIdgruposanguineo", query = "SELECT f FROM Funcionario f WHERE f.idgruposanguineo = :idgruposanguineo")
    , @NamedQuery(name = "Funcionario.findByIdbarrio", query = "SELECT f FROM Funcionario f WHERE f.idbarrio = :idbarrio")
    , @NamedQuery(name = "Funcionario.findByCedula", query = "SELECT f FROM Funcionario f WHERE f.cedula = :cedula")
    , @NamedQuery(name = "Funcionario.findByIdsexo", query = "SELECT f FROM Funcionario f WHERE f.idsexo = :idsexo")
    , @NamedQuery(name = "Funcionario.findByRegistroconducir", query = "SELECT f FROM Funcionario f WHERE f.registroconducir = :registroconducir")
    , @NamedQuery(name = "Funcionario.findByRegistroprofesional", query = "SELECT f FROM Funcionario f WHERE f.registroprofesional = :registroprofesional")
    , @NamedQuery(name = "Funcionario.findByCuentacorriente", query = "SELECT f FROM Funcionario f WHERE f.cuentacorriente = :cuentacorriente")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Funcionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipofuncionario")
    private long idtipofuncionario;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestado")
    private long idestado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idcargo")
    private long idcargo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idfilial")
    private long idfilial;
    @Basic(optional = false)
    @NotNull
    @Column(name = "iddependencia")
    private long iddependencia;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Size(max = 100)
    @Column(name = "direccion")
    private String direccion;
    @Size(max = 100)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 100)
    @Column(name = "celular")
    private String celular;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "email")
    private String email;
    @Size(max = 50)
    @Column(name = "patologia")
    private String patologia;
    @Column(name = "salario")
    private Long salario;
    @Column(name = "fechaingreso")
    @Temporal(TemporalType.DATE)
    private Date fechaingreso;
    @Column(name = "fechaips")
    @Temporal(TemporalType.DATE)
    private Date fechaips;
    @Column(name = "fechasalida")
    @Temporal(TemporalType.DATE)
    private Date fechasalida;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idpais")
    private long idpais;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestadocivil")
    private long idestadocivil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idciudad")
    private long idciudad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idprofesion")
    private long idprofesion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idgruposanguineo")
    private long idgruposanguineo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idbarrio")
    private long idbarrio;
    @Size(max = 12)
    @Column(name = "cedula")
    private String cedula;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsexo")
    private long idsexo;
    @Size(max = 50)
    @Column(name = "registroconducir")
    private String registroconducir;
    @Size(max = 50)
    @Column(name = "registroprofesional")
    private String registroprofesional;
    @Size(max = 50)
    @Column(name = "cuentacorriente")
    private String cuentacorriente;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idfuncionario", fetch = FetchType.LAZY)
    private List<Usuario> usuarioList;

    public Funcionario() {
    }

    public Funcionario(Long id) {
        this.id = id;
    }

    public Funcionario(Long id, String nombre, String apellido, long idtipofuncionario, long idestado, long idcargo, long idfilial, long iddependencia, long idpais, long idestadocivil, long idciudad, long idprofesion, long idgruposanguineo, long idbarrio, long idsexo) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.idtipofuncionario = idtipofuncionario;
        this.idestado = idestado;
        this.idcargo = idcargo;
        this.idfilial = idfilial;
        this.iddependencia = iddependencia;
        this.idpais = idpais;
        this.idestadocivil = idestadocivil;
        this.idciudad = idciudad;
        this.idprofesion = idprofesion;
        this.idgruposanguineo = idgruposanguineo;
        this.idbarrio = idbarrio;
        this.idsexo = idsexo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public long getIdtipofuncionario() {
        return idtipofuncionario;
    }

    public void setIdtipofuncionario(long idtipofuncionario) {
        this.idtipofuncionario = idtipofuncionario;
    }

    public long getIdestado() {
        return idestado;
    }

    public void setIdestado(long idestado) {
        this.idestado = idestado;
    }

    public long getIdcargo() {
        return idcargo;
    }

    public void setIdcargo(long idcargo) {
        this.idcargo = idcargo;
    }

    public long getIdfilial() {
        return idfilial;
    }

    public void setIdfilial(long idfilial) {
        this.idfilial = idfilial;
    }

    public long getIddependencia() {
        return iddependencia;
    }

    public void setIddependencia(long iddependencia) {
        this.iddependencia = iddependencia;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPatologia() {
        return patologia;
    }

    public void setPatologia(String patologia) {
        this.patologia = patologia;
    }

    public Long getSalario() {
        return salario;
    }

    public void setSalario(Long salario) {
        this.salario = salario;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public Date getFechaips() {
        return fechaips;
    }

    public void setFechaips(Date fechaips) {
        this.fechaips = fechaips;
    }

    public Date getFechasalida() {
        return fechasalida;
    }

    public void setFechasalida(Date fechasalida) {
        this.fechasalida = fechasalida;
    }

    public long getIdpais() {
        return idpais;
    }

    public void setIdpais(long idpais) {
        this.idpais = idpais;
    }

    public long getIdestadocivil() {
        return idestadocivil;
    }

    public void setIdestadocivil(long idestadocivil) {
        this.idestadocivil = idestadocivil;
    }

    public long getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(long idciudad) {
        this.idciudad = idciudad;
    }

    public long getIdprofesion() {
        return idprofesion;
    }

    public void setIdprofesion(long idprofesion) {
        this.idprofesion = idprofesion;
    }

    public long getIdgruposanguineo() {
        return idgruposanguineo;
    }

    public void setIdgruposanguineo(long idgruposanguineo) {
        this.idgruposanguineo = idgruposanguineo;
    }

    public long getIdbarrio() {
        return idbarrio;
    }

    public void setIdbarrio(long idbarrio) {
        this.idbarrio = idbarrio;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public long getIdsexo() {
        return idsexo;
    }

    public void setIdsexo(long idsexo) {
        this.idsexo = idsexo;
    }

    public String getRegistroconducir() {
        return registroconducir;
    }

    public void setRegistroconducir(String registroconducir) {
        this.registroconducir = registroconducir;
    }

    public String getRegistroprofesional() {
        return registroprofesional;
    }

    public void setRegistroprofesional(String registroprofesional) {
        this.registroprofesional = registroprofesional;
    }

    public String getCuentacorriente() {
        return cuentacorriente;
    }

    public void setCuentacorriente(String cuentacorriente) {
        this.cuentacorriente = cuentacorriente;
    }

    @XmlTransient
    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Funcionario)) {
            return false;
        }
        Funcionario other = (Funcionario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Funcionario[ id=" + id + " ]";
    }
    
}
