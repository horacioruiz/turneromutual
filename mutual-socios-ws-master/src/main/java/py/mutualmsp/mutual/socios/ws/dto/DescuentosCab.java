/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class DescuentosCab {
        @Getter
        @Setter
        private double totalDescMututal = 0.0;
        @Getter
        @Setter
        private double totalGiraduria = 0.0;
        @Getter
        @Setter
        private double totalVentanilla = 0.0;
        @Getter
        @Setter
        private double totalAsignacion = 0.0;
        @Getter
        @Setter
        private double totalDiferencia = 0.0;
        @Getter
        @Setter
        private List<Descuentos> listaDescuentos;
        
    
}
