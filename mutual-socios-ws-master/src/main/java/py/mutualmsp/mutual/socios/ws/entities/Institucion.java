/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "institucion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Institucion.findAll", query = "SELECT i FROM Institucion i")
    , @NamedQuery(name = "Institucion.findById", query = "SELECT i FROM Institucion i WHERE i.id = :id")
    , @NamedQuery(name = "Institucion.findByDescripcion", query = "SELECT i FROM Institucion i WHERE i.descripcion = :descripcion")
    , @NamedQuery(name = "Institucion.findByIdciudad", query = "SELECT i FROM Institucion i WHERE i.idciudad = :idciudad")
    , @NamedQuery(name = "Institucion.findByIdregional", query = "SELECT i FROM Institucion i WHERE i.idregional = :idregional")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Institucion implements Serializable {

    @OneToMany(mappedBy = "idinstitucion", fetch = FetchType.LAZY)
    private List<SolicitudAyuda> solicitudAyudaList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "descripcion")
    private String descripcion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idciudad")
    private long idciudad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idregional")
    private long idregional;

    public Institucion() {
    }

    public Institucion(Long id) {
        this.id = id;
    }

    public Institucion(Long id, String descripcion, long idciudad, long idregional) {
        this.id = id;
        this.descripcion = descripcion;
        this.idciudad = idciudad;
        this.idregional = idregional;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public long getIdciudad() {
        return idciudad;
    }

    public void setIdciudad(long idciudad) {
        this.idciudad = idciudad;
    }

    public long getIdregional() {
        return idregional;
    }

    public void setIdregional(long idregional) {
        this.idregional = idregional;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Institucion)) {
            return false;
        }
        Institucion other = (Institucion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Institucion[ id=" + id + " ]";
    }

    @XmlTransient
    public List<SolicitudAyuda> getSolicitudAyudaList() {
        return solicitudAyudaList;
    }

    public void setSolicitudAyudaList(List<SolicitudAyuda> solicitudAyudaList) {
        this.solicitudAyudaList = solicitudAyudaList;
    }
    
}
