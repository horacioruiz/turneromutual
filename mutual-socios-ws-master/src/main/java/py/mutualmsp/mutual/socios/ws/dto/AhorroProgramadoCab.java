/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */



public class AhorroProgramadoCab implements Serializable{
    
    
    @Getter
    @Setter
    private Long id;
    
    @Getter
    @Setter
    private Long numeroOperacion;
    
    @Getter
    @Setter
    private String contrato;
    
    @Getter
    @Setter
    private Integer plazo;
    
    @Getter
    @Setter
    private Double tasainteres;
    
    @Getter
    @Setter
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Asuncion")
    private Date fechaOperacion;
    
    @Getter
    @Setter
    private Double importe;
    
    @Getter
    @Setter
    private Double montoAportado;
    
    
}
