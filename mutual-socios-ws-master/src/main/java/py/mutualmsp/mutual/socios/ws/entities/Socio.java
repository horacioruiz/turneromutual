/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author hectorvillalba
 */
@Entity
@Table(name = "socio")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Socio.findAll", query = "SELECT s FROM Socio s"),
    @NamedQuery(name = "Socio.findById", query = "SELECT s FROM Socio s WHERE s.id = :id"),
    @NamedQuery(name = "Socio.findByNombre", query = "SELECT s FROM Socio s WHERE s.nombre = :nombre"),
    @NamedQuery(name = "Socio.findByApellido", query = "SELECT s FROM Socio s WHERE s.apellido = :apellido"),
    @NamedQuery(name = "Socio.findByCedula", query = "SELECT s FROM Socio s WHERE s.cedula = :cedula"),
    @NamedQuery(name = "Socio.findByDireccion", query = "SELECT s FROM Socio s WHERE s.direccion = :direccion"),
    @NamedQuery(name = "Socio.findByNumerocasa", query = "SELECT s FROM Socio s WHERE s.numerocasa = :numerocasa"),
    @NamedQuery(name = "Socio.findByIdbarrio", query = "SELECT s FROM Socio s WHERE s.idbarrio = :idbarrio"),
    @NamedQuery(name = "Socio.findByIdciudadresidencia", query = "SELECT s FROM Socio s WHERE s.idciudadresidencia = :idciudadresidencia"),
    @NamedQuery(name = "Socio.findByTelefonocelular", query = "SELECT s FROM Socio s WHERE s.telefonocelular = :telefonocelular"),
    @NamedQuery(name = "Socio.findByTelefonolineabaja", query = "SELECT s FROM Socio s WHERE s.telefonolineabaja = :telefonolineabaja"),
    @NamedQuery(name = "Socio.findByFechanacimiento", query = "SELECT s FROM Socio s WHERE s.fechanacimiento = :fechanacimiento"),
    @NamedQuery(name = "Socio.findByIdsexo", query = "SELECT s FROM Socio s WHERE s.idsexo = :idsexo"),
    @NamedQuery(name = "Socio.findByIdestadosocio", query = "SELECT s FROM Socio s WHERE s.idestadosocio = :idestadosocio"),
    @NamedQuery(name = "Socio.findByIdestadocivil", query = "SELECT s FROM Socio s WHERE s.idestadocivil = :idestadocivil"),
    @NamedQuery(name = "Socio.findByIdlugarnacimiento", query = "SELECT s FROM Socio s WHERE s.idlugarnacimiento = :idlugarnacimiento"),
    @NamedQuery(name = "Socio.findByIdnacionalidad", query = "SELECT s FROM Socio s WHERE s.idnacionalidad = :idnacionalidad"),
    @NamedQuery(name = "Socio.findByIdprofesion", query = "SELECT s FROM Socio s WHERE s.idprofesion = :idprofesion"),
    @NamedQuery(name = "Socio.findByEmail", query = "SELECT s FROM Socio s WHERE s.email = :email"),
    @NamedQuery(name = "Socio.findByIdtipocasa", query = "SELECT s FROM Socio s WHERE s.idtipocasa = :idtipocasa"),
    @NamedQuery(name = "Socio.findByIdviacobro", query = "SELECT s FROM Socio s WHERE s.idviacobro = :idviacobro"),
    @NamedQuery(name = "Socio.findByDiavencimiento", query = "SELECT s FROM Socio s WHERE s.diavencimiento = :diavencimiento"),
    @NamedQuery(name = "Socio.findByFechaingreso", query = "SELECT s FROM Socio s WHERE s.fechaingreso = :fechaingreso"),
    @NamedQuery(name = "Socio.findByIdpromotor", query = "SELECT s FROM Socio s WHERE s.idpromotor = :idpromotor"),
    @NamedQuery(name = "Socio.findByNumerosocio", query = "SELECT s FROM Socio s WHERE s.numerosocio = :numerosocio"),
    @NamedQuery(name = "Socio.findByRuc", query = "SELECT s FROM Socio s WHERE s.ruc = :ruc"),
    @NamedQuery(name = "Socio.findByDisponibilidad", query = "SELECT s FROM Socio s WHERE s.disponibilidad = :disponibilidad"),
    @NamedQuery(name = "Socio.findByPin", query = "SELECT s FROM Socio s WHERE s.pin = :pin"),
    @NamedQuery(name = "Socio.findByTelefonocelularprincipal", query = "SELECT s FROM Socio s WHERE s.telefonocelularprincipal = :telefonocelularprincipal")})
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Socio implements Serializable {

    @OneToMany(mappedBy = "idsocio", fetch = FetchType.LAZY)
    private List<SolicitudAyuda> solicitudAyudaList;

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Long id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "apellido")
    private String apellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 12)
    @Column(name = "cedula")
    private String cedula;
    @Size(max = 100)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerocasa")
    private int numerocasa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idbarrio")
    private long idbarrio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idciudadresidencia")
    private long idciudadresidencia;
    @Size(max = 100)
    @Column(name = "telefonocelular")
    private String telefonocelular;
    @Size(max = 100)
    @Column(name = "telefonolineabaja")
    private String telefonolineabaja;
    @Column(name = "fechanacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechanacimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idsexo")
    private long idsexo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestadosocio")
    private long idestadosocio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idestadocivil")
    private long idestadocivil;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idlugarnacimiento")
    private long idlugarnacimiento;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idnacionalidad")
    private long idnacionalidad;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idprofesion")
    private long idprofesion;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 50)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idtipocasa")
    private long idtipocasa;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idviacobro")
    private long idviacobro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "diavencimiento")
    private long diavencimiento;
    @Column(name = "fechaingreso")
    @Temporal(TemporalType.DATE)
    private Date fechaingreso;
    @Basic(optional = false)
    @NotNull
    @Column(name = "idpromotor")
    private long idpromotor;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numerosocio")
    private long numerosocio;
    @Size(max = 12)
    @Column(name = "ruc")
    private String ruc;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Column(name = "disponibilidad")
    private BigDecimal disponibilidad;
    @Size(max = 20)
    @Column(name = "pin")
    private String pin;
    @Size(max = 11)
    @Column(name = "telefonocelularprincipal")
    private String telefonocelularprincipal;
    
    @Column(name = "fcm_token")
    private String fcmToken;
    
    @Column(name = "pin_acceso_mutual")
    private String pinAccesoMutual;
    
    @Column(name = "acceso_mutual")
    private Boolean accesoMutual;

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getPinAccesoMutual() {
        return pinAccesoMutual;
    }

    public void setPinAccesoMutual(String pinAccesoMutual) {
        this.pinAccesoMutual = pinAccesoMutual;
    }

    public Boolean getAccesoMutual() {
        return accesoMutual;
    }

    public void setAccesoMutual(Boolean accesoMutual) {
        this.accesoMutual = accesoMutual;
    }
    
    
    
    public Socio() {
    }

    public Socio(Long id) {
        this.id = id;
    }

    public Socio(Long id, String nombre, String apellido, String cedula, int numerocasa, long idbarrio, long idciudadresidencia, long idsexo, long idestadosocio, long idestadocivil, long idlugarnacimiento, long idnacionalidad, long idprofesion, long idtipocasa, long idviacobro, long diavencimiento, long idpromotor, long numerosocio, BigDecimal disponibilidad) {
        this.id = id;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cedula = cedula;
        this.numerocasa = numerocasa;
        this.idbarrio = idbarrio;
        this.idciudadresidencia = idciudadresidencia;
        this.idsexo = idsexo;
        this.idestadosocio = idestadosocio;
        this.idestadocivil = idestadocivil;
        this.idlugarnacimiento = idlugarnacimiento;
        this.idnacionalidad = idnacionalidad;
        this.idprofesion = idprofesion;
        this.idtipocasa = idtipocasa;
        this.idviacobro = idviacobro;
        this.diavencimiento = diavencimiento;
        this.idpromotor = idpromotor;
        this.numerosocio = numerosocio;
        this.disponibilidad = disponibilidad;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getNumerocasa() {
        return numerocasa;
    }

    public void setNumerocasa(int numerocasa) {
        this.numerocasa = numerocasa;
    }

    public long getIdbarrio() {
        return idbarrio;
    }

    public void setIdbarrio(long idbarrio) {
        this.idbarrio = idbarrio;
    }

    public long getIdciudadresidencia() {
        return idciudadresidencia;
    }

    public void setIdciudadresidencia(long idciudadresidencia) {
        this.idciudadresidencia = idciudadresidencia;
    }

    public String getTelefonocelular() {
        return telefonocelular;
    }

    public void setTelefonocelular(String telefonocelular) {
        this.telefonocelular = telefonocelular;
    }

    public String getTelefonolineabaja() {
        return telefonolineabaja;
    }

    public void setTelefonolineabaja(String telefonolineabaja) {
        this.telefonolineabaja = telefonolineabaja;
    }

    public Date getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(Date fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public long getIdsexo() {
        return idsexo;
    }

    public void setIdsexo(long idsexo) {
        this.idsexo = idsexo;
    }

    public long getIdestadosocio() {
        return idestadosocio;
    }

    public void setIdestadosocio(long idestadosocio) {
        this.idestadosocio = idestadosocio;
    }

    public long getIdestadocivil() {
        return idestadocivil;
    }

    public void setIdestadocivil(long idestadocivil) {
        this.idestadocivil = idestadocivil;
    }

    public long getIdlugarnacimiento() {
        return idlugarnacimiento;
    }

    public void setIdlugarnacimiento(long idlugarnacimiento) {
        this.idlugarnacimiento = idlugarnacimiento;
    }

    public long getIdnacionalidad() {
        return idnacionalidad;
    }

    public void setIdnacionalidad(long idnacionalidad) {
        this.idnacionalidad = idnacionalidad;
    }

    public long getIdprofesion() {
        return idprofesion;
    }

    public void setIdprofesion(long idprofesion) {
        this.idprofesion = idprofesion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getIdtipocasa() {
        return idtipocasa;
    }

    public void setIdtipocasa(long idtipocasa) {
        this.idtipocasa = idtipocasa;
    }

    public long getIdviacobro() {
        return idviacobro;
    }

    public void setIdviacobro(long idviacobro) {
        this.idviacobro = idviacobro;
    }

    public long getDiavencimiento() {
        return diavencimiento;
    }

    public void setDiavencimiento(long diavencimiento) {
        this.diavencimiento = diavencimiento;
    }

    public Date getFechaingreso() {
        return fechaingreso;
    }

    public void setFechaingreso(Date fechaingreso) {
        this.fechaingreso = fechaingreso;
    }

    public long getIdpromotor() {
        return idpromotor;
    }

    public void setIdpromotor(long idpromotor) {
        this.idpromotor = idpromotor;
    }

    public long getNumerosocio() {
        return numerosocio;
    }

    public void setNumerosocio(long numerosocio) {
        this.numerosocio = numerosocio;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public BigDecimal getDisponibilidad() {
        return disponibilidad;
    }

    public void setDisponibilidad(BigDecimal disponibilidad) {
        this.disponibilidad = disponibilidad;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getTelefonocelularprincipal() {
        return telefonocelularprincipal;
    }

    public void setTelefonocelularprincipal(String telefonocelularprincipal) {
        this.telefonocelularprincipal = telefonocelularprincipal;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Socio)) {
            return false;
        }
        Socio other = (Socio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.mutualmsp.mutual.socios.ws.entities.Socio[ id=" + id + " ]";
    }

    @XmlTransient
    public List<SolicitudAyuda> getSolicitudAyudaList() {
        return solicitudAyudaList;
    }

    public void setSolicitudAyudaList(List<SolicitudAyuda> solicitudAyudaList) {
        this.solicitudAyudaList = solicitudAyudaList;
    }
    
}
