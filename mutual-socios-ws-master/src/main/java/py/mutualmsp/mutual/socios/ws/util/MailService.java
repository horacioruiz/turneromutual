package py.mutualmsp.mutual.socios.ws.util;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.util.ByteArrayDataSource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.java.Log;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailAttachment;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;
import py.mutualmsp.mutual.socios.ws.entities.Socio;

/**
 *
 * @author hectorvillalba
 * metodo que envia mail
 */
@Log
@Stateless
public class MailService implements Serializable{
    
    @PersistenceContext
    EntityManager em;
    
    public  void enviarMailActivacion(Socio socio) throws EmailException, IOException {
        try {
            //String link = Constants.PUBLIC_API_CONFIRM_URL + donante.getNombre() + " "+ donante.getApellido() + "/" + usuarioParam.getTokenConfirmacion();
            String subject = "Bienvenido a la Mutual MSP y BS de Funcionarios de Salud";
            //String password = RandomString.getAlphaNumericString(4);
            String message = "Para activar su cuenta por favor haga click en el siguiente enlace: " 
                    + Constants.PUBLIC_API_URL + "registro/activar-cuenta/" + socio.getId();
            
//            donante.setPassword(PasswordService.getInstance().encrypt(password));
//            em.merge(donante);
            
            List mails = new ArrayList();
            mails.add(socio.getEmail());

            sendeEmail(subject, message, mails, "Mutual Móvil");
        } catch (Exception ex) {
            Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public  void enviarPinAcceso(Socio socio) throws EmailException, IOException {
        try {
            //String link = Constants.PUBLIC_API_CONFIRM_URL + donante.getNombre() + " "+ donante.getApellido() + "/" + usuarioParam.getTokenConfirmacion();
            String subject = "Bienvenido a la Mutual MSP y BS de Funcionarios de Salud";
            String password = RandomString.getAlphaNumericString(4);
            String message = "Su password provisorio es: " + password;
            
            socio.setPinAccesoMutual(PasswordService.getInstance().encrypt(password));
            em.merge(socio);
            
            List mails = new ArrayList();
            mails.add(socio.getEmail());

            sendeEmail(subject, message, mails,"Mutual Móvil");
        } catch (Exception ex) {
            Logger.getLogger(MailService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    

    public  void sendeEmail(String subject,
            String mensaje,
            List<String> emails,
            byte[] content,
            String nameAttachment,String fromApp
    ) throws EmailException, IOException {
        EmailAttachment attachment = new EmailAttachment();
        attachment.setDisposition(EmailAttachment.ATTACHMENT);
        attachment.setDescription("Archivo Adjunto");
        attachment.setName(nameAttachment);

        // Create the email message
        MultiPartEmail email = new MultiPartEmail();
        email.setHostName(Constants.HOST_NAME_SMTP);
        email.setSSLOnConnect(true);
        email.setSmtpPort(465);
        email.setAuthenticator(new DefaultAuthenticator(Constants.USER_NAME_SMTP, Constants.PASS_SMTP));
        for (String s : emails) {
            email.addTo(s);
        }
        //email.addCc(config.getMailCC());
        email.setFrom(Constants.USER_NAME_SMTP, fromApp);
        email.setSubject(subject);
        email.setMsg(mensaje);

        // add the attachment
        String extension = nameAttachment.substring(nameAttachment.length() - 3);
        String nameWithoutExtension = nameAttachment.substring(0, nameAttachment.length() - 4);
        email.attach(new ByteArrayDataSource(content, "application/" + extension), 
                nameAttachment, "Archivo Adjunto", 
                EmailAttachment.ATTACHMENT);
        // send the email
        email.send();
    }

    public  void sendeEmail(String subject,
            String mensaje,
            List<String> emails, String fromApp) throws EmailException, IOException {

        // Create the email message
        Email email = new SimpleEmail();
        email.setCharset("UTF-8");
        email.setHostName(Constants.HOST_NAME_SMTP);
        email.setSmtpPort(465);
        email.setSSLOnConnect(true);
        email.setAuthenticator(new DefaultAuthenticator(Constants.USER_NAME_SMTP, Constants.PASS_SMTP));
        for (String s : emails) {
            email.addTo(s);
        }
        //email.addCc(config.getMailCC());
        email.setFrom(Constants.USER_NAME_SMTP, fromApp);
        email.setSubject(subject);
        email.setMsg(mensaje);
        // send the email
        email.send();
    }
}
