/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.mutualmsp.mutual.socios.ws.dto;

import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author hectorvillalba
 */
public class SociosDto implements Serializable {
    
    @Getter
    @Setter
    private Long id;
    @Getter
    @Setter    
    private String nombre;
    @Getter
    @Setter    
    private String apellido;
    @Getter
    @Setter    
    private String cedula;
    @Getter
    @Setter    
    private String email;
    @Getter
    @Setter    
    private String telefono;
    @Getter
    @Setter    
    private String fcmToken;
    @Getter
    @Setter    
    private String pinMutual;
    @Getter
    @Setter        
    private Boolean accesoMutual;
    
    
}
