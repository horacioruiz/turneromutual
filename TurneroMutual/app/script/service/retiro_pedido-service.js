'use strict'
appLogin
        .factory('RetiroPedidoService', function ($http, config) {
            var url = config.backend + "/retiroPedido";
            return {
                listar: function () {
                    return $http.get(url + "/full");
                },
                listarPorSerie: function (serie) {
                    return $http.get(url + "/listarPorSerie/" + serie);
                },
                listarFullListoPorSerie: function (serie, box) {
                    return $http.get(config.backendHere + "/retiroPedido/listarFullListo/" + serie + "/" + box);
                },
                listarLlamados: function () {
                    return $http.get(config.backendHere + "/retiroPedido/listarLlamados");
                },
                listarFullPreparacionSerie: function (serie, box) {
                    return $http.get(config.backendHere + "/retiroPedido/listarFullPreparacionSerie/" + serie + "/" + box);
                },
                listarPorId: function (id) {
                    return $http.get(url + "/" + id);
                },
                listarDetalleFactura: function (id) {
                    return $http.get(config.backend + "/facturaClienteDet/listFacturaCab/" + id);
                },
                imprimirTicket: function (descri, numero, nombre, ci) {
                    return $http.get("http://localhost:8080/api/imprimirTicket/" + descri + "/" + numero + "/" + nombre + "/" + ci);
                },
                buscarPorNumeroPedido: function (num) {
                    return $http.get(url + "/buscarPorNumeroPedido/" + num);
                },
                listarPreparacion: function () {
                    return $http.get(url + "/listarPreparacion");
                },
                listarFullPreparacion: function () {
                    return $http.get(url + "/listarFullPreparacion");
                },
                listarListo: function () {
                    return $http.get(config.backendHere + "/retiroPedido/listarListo");
                },
                listarFullListo: function () {
                    return $http.get(url + "/listarFullListo");
                },
                actualizarPreparado: function (id) {
                    return $http.get(url + "/actualizarPreparado/" + id);
                },
                actualizarListo: function (id) {
                    return $http.get(config.backendHere + "/retiroPedido/actualizarListo/" + id);
                },
                actualizarEntregado: function (id, idFunc) {
                    return $http.get(config.backendHere + "/retiroPedido/actualizarEntregado/" + id + "/" + idFunc);
                },
                registrarPersona: function (ci, nombre, apellido) {
                    return $http.get(config.backendHere + "/retiroPedido/registrarPersona/" + ci + "/" + nombre + "/" + apellido);
                },
                listarAnoPeriodo: function () {
                    return $http.get(config.backendHere + "/retiroPedido/listarAnoPeriodo");
                },
                actualizarRecall: function (id, box) {
                    return $http.get(config.backendHere + "/retiroPedido/actualizarRecall/" + id + "/" + box);
                },
                listarUsuario: function (id) {
                    return $http.get(url + "/listarUsuario/" + id);
                },
                listarPorIdUsuario: function (id) {
                    return $http.get(url + "/idUsuario/" + id);
                },
                restauraPassPorIdUsuario: function (id) {
                    return $http.get(url + "/restauraPassPorIdUsuario/" + id);
                },
                restauraEmailPorIdUsuario: function (id) {
                    return $http.get(url + "/restauraEmailPorIdUsuario/" + id);
                },
                crear: function (data) {
                    return $http.post(url, data);
                },
                crearPorIdUsuario: function (id) {
                    return $http.get(url + "/crearPorIdUsuario/" + id);
                },
                actualizarPorIdUsuario: function (id) {
                    return $http.get(url + "/actualizarPorIdUsuario/" + id);
                },
                actualizar: function (data) {
                    return $http.put(url, data);
                },
                eliminar: function (id) {
                    return $http.delete(url + "/" + id);
                }
            };
        });